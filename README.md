# pyakalabeth

## Intro

This is port of the original Akalabeth code from Applesoft BASIC to Python.

The original source code is in included in the AKALABETH.TXT file.

## Dependencies

You have to install pyxel and scipy:

    pip install pyxel scipy

## Running

Just run the main script:

    python src/akalabeth/main.py

The original game can be played online in apple2jse here: https://www.scullinsteel.com/apple/e#akalabeth


## Keyboard

These keys are used:

- ESC - Closes the game at any time without warning
- SPACE - Used for resurrection / inside the castle / to pass on this turn
- Arrow keys - Direction of movement, inside of dungeons down will turn the player around
- X - Enter shop / castle / dungeon and climb ladders up or down / fall down holes
- A - Attack monster / use magic amulet
- Q - Quit from shop
- The first letter of an item in the shop will buy it (F)OOD / (A)XE / (M)AGIC AMULET / etc.
- S - Show stats / inventory
- P - Toggle PAUSE mode
