from stats import Stats
from shop import Shop


# noinspection PyMethodMayBeStatic
class Init:
    def __init__(self, bas, main, skip_char_generator):
        self.bas = bas
        self.main = main
        self.skip_char_generator = skip_char_generator

    def run(self):
        self.bas.text()
        self.bas.home()
        self.input_lucky_number()
        self.input_level_of_play()
        self.load_character_attributes()
        self.load_monsters()
        self.choose_player_qualities()
        self.choose_character_class()
        self.load_items()
        self.display_stats_and_inventory()
        if self.skip_char_generator:
            self.main.player_items[0] = 5000
            return
        self.shop()

    def choose_character_class(self):
        if self.skip_char_generator:
            self.main.player_class = 'F'
            return
        while True:
            self.bas.vtab(15)
            self.bas.print()
            self.bas.print('AND SHALT THOU BE A FIGHTER OR A MAGE?')
            self.bas.htab(20)
            self.main.player_class = self.bas.get()
            if self.main.player_class == 'M' or self.main.player_class == 'F':
                break

    def load_items(self):
        self.main.item_names = [
            'FOOD',
            'RAPIER',
            'AXE',
            'SHIELD',
            'BOW AND ARROWS',
            'MAGIC AMULET'
        ]

    def choose_player_qualities(self):
        if self.skip_char_generator:
            for x in range(0, 6):
                self.main.character_attribute_values[x] = 1800
            return
        while True:
            for x in range(0, 6):
                attribute = self.get_random_attribute_value()
                self.main.character_attribute_values[x] = attribute
            self.bas.home()
            self.bas.vtab(8)
            for x in range(0, 6):
                self.bas.print(f'{self.main.character_attribute_names[x]}{self.main.character_attribute_values[x]}')
            self.bas.print()
            self.bas.print('SHALT THOU PLAY WITH THESE QUALITIES?')
            self.bas.htab(20)
            player_input = self.bas.get()
            if player_input == 'Y':
                break

    def get_random_attribute_value(self):
        # Random int value between 4 and 25
        attribute = self.bas.int(self.bas.sqr(self.bas.rnd(1)) * 21 + 4)
        return attribute

    def load_monsters(self):
        self.main.monster_names = [
            'SKELETON',
            'THIEF',
            'GIANT RAT',
            'ORC',
            'VIPER',
            'CARRION CRAWLER',
            'GREMLIN',
            'MIMIC',
            'DAEMON',
            'BALROG'
        ]

    def load_character_attributes(self):
        self.main.character_attribute_names = [
            'HIT POINTS.....',
            'STRENGTH.......',
            'DEXTERITY......',
            'STAMINA........',
            'WISDOM.........',
            'GOLD...........'
        ]

    def input_level_of_play(self):
        if self.skip_char_generator:
            self.main.difficulty = 1
            return
        while True:
            self.bas.vtab(7)
            player_input2 = self.bas.input('LEVEL OF PLAY (1-10)......')
            self.main.difficulty = self.bas.int(self.bas.val(player_input2))
            if 1 <= self.main.difficulty <= 10:
                break

    def input_lucky_number(self):
        if self.skip_char_generator:
            self.main.lucky_number = 42
            return
        self.bas.vtab(5)
        player_input1 = self.bas.input('TYPE THY LUCKY NUMBER.....')
        self.main.lucky_number = self.bas.val(player_input1)

    def display_stats_and_inventory(self):
        # 60080 - 60130
        stats = Stats(self.bas, self.main)
        stats.run()

    def shop(self):
        # 60200 - 60250
        shop = Shop(self.bas, self.main)
        shop.run()
