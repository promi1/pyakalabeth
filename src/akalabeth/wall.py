def draw_left_corridor(plot, point, perspective_1, perspective_2):
    plot([
        point(perspective_1.left, perspective_1.top),
        point(perspective_1.left, perspective_1.bottom)
    ])
    plot([
        point(perspective_1.left, perspective_2.top),
        point(perspective_2.left, perspective_2.top),
        point(perspective_2.left, perspective_2.bottom),
        point(perspective_1.left, perspective_2.bottom)
    ])


def draw_right_corridor(plot, point, perspective_1, perspective_2):
    plot([
        point(perspective_1.right, perspective_1.top),
        point(perspective_1.right, perspective_1.bottom)
    ])
    plot([
        point(perspective_1.right, perspective_2.top),
        point(perspective_2.right, perspective_2.top),
        point(perspective_2.right, perspective_2.bottom),
        point(perspective_1.right, perspective_2.bottom)
    ])


def draw_center_wall(plot, point, perspective):
    plot([
        point(perspective.left, perspective.top),
        point(perspective.right, perspective.top),
        point(perspective.right, perspective.bottom),
        point(perspective.left, perspective.bottom),
        point(perspective.left, perspective.top)
    ])


def draw_left_wall_segment(plot, point, perspective_1, perspective_2):
    plot([
        point(perspective_1.left, perspective_1.top),
        point(perspective_2.left, perspective_2.top)
    ])
    plot([
        point(perspective_1.left, perspective_1.bottom),
        point(perspective_2.left, perspective_2.bottom)
    ])


def draw_right_wall_segment(plot, point, perspective_1, perspective_2):
    plot([
        point(perspective_1.right, perspective_1.top),
        point(perspective_2.right, perspective_2.top)
    ])
    plot([
        point(perspective_1.right, perspective_1.bottom),
        point(perspective_2.right, perspective_2.bottom)
    ])
