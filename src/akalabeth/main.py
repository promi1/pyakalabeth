import threading

import pyxel

from basic import Basic, VideoMode
from akalabeth import Akalabeth
from dungeon import Dungeon
from dungeon_map_pair import DungeonMapPair
from dungeon_object import DungeonObject
from dungeon_renderer import DungeonRenderer
from world_object import WorldObject


def update():
    global bas
    if pyxel.btnp(pyxel.KEY_0):
        bas.key = '0'
    elif pyxel.btnp(pyxel.KEY_1):
        bas.key = '1'
    elif pyxel.btnp(pyxel.KEY_2):
        bas.key = '2'
    elif pyxel.btnp(pyxel.KEY_3):
        bas.key = '3'
    elif pyxel.btnp(pyxel.KEY_4):
        bas.key = '4'
    elif pyxel.btnp(pyxel.KEY_5):
        bas.key = '5'
    elif pyxel.btnp(pyxel.KEY_6):
        bas.key = '6'
    elif pyxel.btnp(pyxel.KEY_7):
        bas.key = '7'
    elif pyxel.btnp(pyxel.KEY_8):
        bas.key = '8'
    elif pyxel.btnp(pyxel.KEY_9):
        bas.key = '9'
    elif pyxel.btnp(pyxel.KEY_A):
        bas.key = 'A'
    elif pyxel.btnp(pyxel.KEY_B):
        bas.key = 'B'
    elif pyxel.btnp(pyxel.KEY_C):
        bas.key = 'C'
    elif pyxel.btnp(pyxel.KEY_D):
        bas.key = 'D'
    elif pyxel.btnp(pyxel.KEY_E):
        bas.key = 'E'
    elif pyxel.btnp(pyxel.KEY_F):
        bas.key = 'F'
    elif pyxel.btnp(pyxel.KEY_G):
        bas.key = 'G'
    elif pyxel.btnp(pyxel.KEY_H):
        bas.key = 'H'
    elif pyxel.btnp(pyxel.KEY_I):
        bas.key = 'I'
    elif pyxel.btnp(pyxel.KEY_J):
        bas.key = 'J'
    elif pyxel.btnp(pyxel.KEY_K):
        bas.key = 'K'
    elif pyxel.btnp(pyxel.KEY_L):
        bas.key = 'L'
    elif pyxel.btnp(pyxel.KEY_M):
        bas.key = 'M'
    elif pyxel.btnp(pyxel.KEY_N):
        bas.key = 'N'
    elif pyxel.btnp(pyxel.KEY_O):
        bas.key = 'O'
    elif pyxel.btnp(pyxel.KEY_P):
        bas.key = 'P'
    elif pyxel.btnp(pyxel.KEY_Q):
        bas.key = 'Q'
    elif pyxel.btnp(pyxel.KEY_R):
        bas.key = 'R'
    elif pyxel.btnp(pyxel.KEY_S):
        bas.key = 'S'
    elif pyxel.btnp(pyxel.KEY_T):
        bas.key = 'T'
    elif pyxel.btnp(pyxel.KEY_U):
        bas.key = 'U'
    elif pyxel.btnp(pyxel.KEY_V):
        bas.key = 'V'
    elif pyxel.btnp(pyxel.KEY_W):
        bas.key = 'W'
    elif pyxel.btnp(pyxel.KEY_X):
        bas.key = 'X'
    elif pyxel.btnp(pyxel.KEY_Y):
        bas.key = 'Y'
    elif pyxel.btnp(pyxel.KEY_Z):
        bas.key = 'Z'
    elif pyxel.btnp(pyxel.KEY_RETURN):
        bas.key = '\n'
    elif pyxel.btnp(pyxel.KEY_ESCAPE):
        bas.key = '␛'
    elif pyxel.btnp(pyxel.KEY_UP):
        bas.key = '↑'
    elif pyxel.btnp(pyxel.KEY_DOWN):
        bas.key = '↓'
    elif pyxel.btnp(pyxel.KEY_LEFT):
        bas.key = '←'
    elif pyxel.btnp(pyxel.KEY_RIGHT):
        bas.key = '→'
    elif pyxel.btnp(pyxel.KEY_SPACE):
        bas.key = ' '


def draw():
    global bas
    pyxel.cls(0)
    if bas.video_mode == VideoMode.TEXT:
        for y in range(0, bas.text_buffer_lines):
            for x in range(0, bas.text_buffer_columns):
                if bas.text_buffer[y][x] != ' ':
                    pyxel.text(8 + x * 7, 8 + y * 8, bas.text_buffer[y][x], 13)
    elif bas.video_mode == VideoMode.HGR:
        for y in range(0, bas.hgr_buffer_height):
            for x in range(0, bas.hgr_buffer_width):
                pyxel.pset(x, y, bas.hgr_buffer[y][x])
        for y in range(bas.text_buffer_lines - 4, bas.text_buffer_lines):
            for x in range(0, bas.text_buffer_columns):
                if bas.text_buffer[y][x] != ' ':
                    pyxel.text(8 + x * 7, 8 + y * 8, bas.text_buffer[y][x], 13)
        if minimap:
            for x in range(0, 11):
                for y in range(0, 11):
                    obj = ' '
                    match dungeon_map[x][y].obj:
                        case DungeonObject.WALL:
                            obj = '+'
                        case DungeonObject.NORMAL_DOOR:
                            obj = 'D'
                        case DungeonObject.HIDDEN_DOOR:
                            obj = 'H'
                    if aka.dungeon_player_pos_x == x and aka.dungeon_player_pos_y == y:
                        obj = 'P'
                    pyxel.text(300 + x * 7, 8 + y * 8, obj, 13)


if __name__ == '__main__':
    # Cheats
    minimap = False
    powerful_char = False
    bas = Basic()
    # Terrain 20x20 tiles
    world = [[WorldObject.EMPTY] * 21 for i in range(0, 21)]
    # Dungeon 10x10 tiles
    dungeon_map = [[DungeonMapPair(DungeonObject.EMPTY0, 0) for _ in range(0, 11)] for _ in range(0, 11)]
    dungeon = Dungeon(dungeon_map)
    monster_names = [''] * 10
    dungeon_renderer = DungeonRenderer(bas, dungeon, monster_names)
    aka = Akalabeth(bas, world, dungeon, monster_names, dungeon_renderer, powerful_char)
    thread = threading.Thread(target=aka.run, args=())
    thread.daemon = True
    thread.start()
    width = 296
    if minimap:
        width = width + 100
    pyxel.init(width, 208, title='Akalabeth - Dungeon of Doom', display_scale=3)
    pyxel.run(update, draw)
