class Fight:
    def __init__(self, bas, main, dungeon):
        self.bas = bas
        self.main = main
        self.dungeon = dungeon

    def run(self):
        damage = 0
        selection = False
        while not selection:
            selection = True
            damage = 0
            self.bas.print('ATTACK')
            self.bas.print_no_newline('WHICH WEAPON ')
            q = self.bas.get()
            if q == 'R':
                damage = 10
                self.bas.print('RAPIER')
                if self.main.player_items[1] < 1:
                    self.bas.print('NOT OWNED')
                    selection = False
            if q == 'A':
                damage = 5
                self.bas.print('AXE')
                if self.main.player_items[2] < 1:
                    self.bas.print('NOT OWNED')
                    selection = False
            if q == 'S':
                damage = 1
                self.bas.print('SHIELD')
                if self.main.player_items[3] < 1:
                    self.bas.print('NOT OWNED')
                    selection = False
            if q == 'B':
                damage = 4
                self.bas.print('BOW')
                if self.main.player_items[4] < 1:
                    self.bas.print('NOT OWNED')
                    selection = False
            if q == 'M':
                self.bas.print('MAGIC AMULET')
                if self.main.player_items[5] < 1:
                    self.bas.print('NONE OWNED')
                    selection = False
                else:
                    self.use_amulet()
                    return
            if q == 'B' and self.main.player_class == 'M':
                self.bas.print("MAGES CAN'T USE BOWS!")
                selection = False
            if q == 'R' and self.main.player_class == 'M':
                self.bas.print("MAGES CAN'T USE RAPIERS!")
                selection = False
        if damage == 0:
            self.bas.print('HANDS')
        if damage == 5 or damage == 4:
            self.attack_with_axe_or_bow(damage)
        else:
            self.attack_melee(damage)

    def attack_melee(self, damage):
        # 1661
        mn = self.dungeon.map[self.main.dungeon_player_pos_x + self.main.dungeon_player_dir_x][self.main.dungeon_player_pos_y + self.main.dungeon_player_dir_y].monster
        self.attack_monster(mn, damage)

    def attack_monster(self, monster, damage):
        # 1662
        if monster < 1 or self.main.character_attribute_values[2] - self.bas.rnd(1) * 25 < monster + self.dungeon.level:
            self.miss()
        else:
            self.hit_monster(monster, damage)

    def miss(self):
        self.bas.print('YOU MISSED')
        self.finish_attack()

    def hit_monster(self, monster, damage):
        self.bas.print('HIT!!! ')
        damage = self.bas.rnd(1) * damage + self.main.character_attribute_values[1] / 5
        self.dungeon.monsters[monster][1] -= damage
        self.bas.print(f"{self.main.monster_names[monster - 1]}'S HIT POINTS={self.dungeon.monsters[monster][1]}")
        if self.dungeon.monsters[monster][1] < 1:
            self.bas.print(f'THOU HAST KILLED A {self.main.monster_names[monster - 1]}')
            self.bas.print('THOU SHALT RECEIVE')
            da = self.bas.int(monster + self.dungeon.level)
            self.bas.print(f'{da} PIECES OF EIGHT')
            self.main.character_attribute_values[5] = self.bas.int(self.main.character_attribute_values[5] + da)
            monster_location = self.dungeon.monster_locations[monster]
            self.dungeon.map[monster_location[0]][monster_location[1]].monster = 0
            self.dungeon.monsters[monster][0] = 0
        self.main.dungeon_bonus_hit_points += self.bas.int(monster * self.dungeon.level / 2)
        if monster == self.main.quest_monster:
            self.main.quest_monster = -self.main.quest_monster
        self.finish_attack()

    def finish_attack(self):
        # 1668
        if self.main.pause_after_action:
            self.bas.print_no_newline('-CR- TO CONT. ')
            self.bas.input()

    def attack_with_axe_or_bow(self, damage):
        # 1670
        if damage == 5:
            self.bas.print_no_newline('TO THROW OR SWING:')
            q = self.bas.get()
            if q != "T":
                self.bas.print('SWING')
                self.attack_melee(damage)
                return
        if damage == 5:
            self.bas.print('THROW')
            self.main.player_items[2] -= 1
            self.attack_ranged(damage)
            return
        self.attack_ranged(damage)

    def attack_ranged(self, damage):
        # 1672
        for y in range(1, 6):
            tmp1 = self.main.dungeon_player_pos_x + self.main.dungeon_player_dir_x * y
            tmp2 = self.main.dungeon_player_pos_y + self.main.dungeon_player_dir_y * y
            if tmp1 < 1 or tmp1 > 9 or tmp2 > 9 or tmp2 < 0:
                # Out of bounds
                self.miss()
                return
            mn = self.dungeon.map[tmp1][tmp2].monster
            if mn > 0:
                # There is a monster, attack it
                self.attack_monster(mn, damage)
                return
        # No monster found
        self.miss()

    def use_amulet(self):
        # 1681
        if self.main.player_class == "F":
            effect = self.get_random_amulet_effect()
        else:
            effect = self.choose_amulet_effect()
            self.deplete_amulet()
        self.use_amulet_effect(effect)

    def get_random_amulet_effect(self):
        return self.bas.int(self.bas.rnd(1) * 4 + 1)

    def deplete_amulet(self):
        if self.bas.rnd(1) > .75:
            self.bas.print('LAST CHARGE ON THIS AMULET!')
            self.main.player_items[5] -= 1

    def choose_amulet_effect(self):
        while True:
            self.bas.print('1-LADDER-UP', '2-LADDER-DN')
            self.bas.print('3-KILL', '4-BAD??')
            self.bas.print_no_newline('CHOICE ')
            effect = self.bas.get()
            self.bas.print(effect)
            if not (effect < 1 or effect > 4):
                break
        return effect

    def use_amulet_effect(self, effect):
        match effect:
            case 1:
                self.bas.print('LADDER UP')
                self.dungeon.map[self.main.dungeon_player_pos_x][self.main.dungeon_player_pos_y].obj = DungeonObject.UP_LADDER
            case 2:
                self.bas.print('LADDER DOWN')
                self.dungeon.map[self.main.dungeon_player_pos_x][self.main.dungeon_player_pos_y].obj = DungeonObject.DOWN_LADDER
            case 3:
                self.bas.print('MAGIC ATTACK')
                dam = 10 + self.dungeon.level
                self.attack_ranged(dam)
            case 4:
                self.apply_amulet_special_effect()

    def apply_amulet_special_effect(self):
        # 1692
        match self.get_random_amulet_special_effect():
            case 1:
                self.turn_player_into_toad()
            case 2:
                self.turn_player_into_lizard_man()
            case 3:
                self.backfire()

    def get_random_amulet_special_effect(self):
        return self.bas.int(self.bas.rnd(1) * 3 + 1)

    def turn_player_into_toad(self):
        self.bas.print('YOU HAVE BEEN TURNED')
        self.bas.print('INTO A TOAD!')
        for i in range(1, 5):
            self.main.character_attribute_values[i] = 3

    def turn_player_into_lizard_man(self):
        self.bas.print('YOU HAVE BEEN TURNED')
        self.bas.print('INTO A LIZARD MAN')
        for i in range(0, 5):
            self.main.character_attribute_values[i] = self.bas.int(self.main.character_attribute_values[i] * 2.5)

    def backfire(self):
        self.bas.print('BACKFIRE')
        self.main.character_attribute_values[0] //= 2
