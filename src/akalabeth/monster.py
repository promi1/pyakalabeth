from constants import *


def draw_monster(plot, point, monster, x, y, perspective):
    dis = perspective.distance
    match monster:
        case 1:
            _draw_skeleton(plot, point, x, y, dis)
        case 2:
            _draw_thief(plot, point, x, y, dis)
        case 3:
            _draw_giant_rat(plot, point, x, y, dis)
        case 4:
            _draw_orc(plot, point, x, y, dis)
        case 5:
            _draw_viper(plot, point, x, y, dis)
        case 6:
            _draw_carrion_crawler(plot, point, x, y, perspective)
        case 7:
            _draw_gremlin(plot, point, x, y, dis)
        case 8:
            _draw_mimic(plot, point, x, y, perspective)
        case 9:
            _draw_daemon(plot, point, x, y, dis)
        case 10:
            _draw_balrog(plot, point, x, y, dis)


def _draw_skeleton(plot, point, x, y, dis):
    plot([
        point(x - 23 // dis, y),
        point(x - 15 // dis, y),
        point(x - 15 // dis, y - 15 // dis),
        point(x - 8 // dis, y - 30 // dis),
        point(x + 8 // dis, y - 30 // dis),
        point(x + 15 // dis, y - 15 // dis),
        point(x + 15 // dis, y),
        point(x + 23 // dis, y)
    ])
    plot([
        point(x, y - 26 // dis),
        point(x, y - 65 // dis)
    ])
    plot([
        point(int(x - 2 / dis + .5), y - 38 // dis),
        point(int(x + 2 / dis + .5), y - 38 // dis)
    ])
    plot([
        point(int(x - 3 / dis + .5), y - 45 // dis),
        point(int(x + 3 / dis + .5), y - 45 // dis)
    ])
    plot([
        point(int(x - 5 / dis + .5), y - 53 // dis),
        point(int(x + 5 / dis + .5), y - 53 // dis)
    ])
    plot([
        point(x - 23 // dis, y - 56 // dis),
        point(x - 30 // dis, y - 53 // dis),
        point(x - 23 // dis, y - 45 // dis),
        point(x - 23 // dis, y - 53 // dis),
        point(x - 8 // dis, y - 38 // dis)
    ])
    plot([
        point(x - 15 // dis, y - 45 // dis),
        point(x - 8 // dis, y - 60 // dis),
        point(x + 8 // dis, y - 60 // dis),
        point(x + 15 // dis, y - 45 // dis)
    ])
    plot([
        point(x + 15 // dis, y - 42 // dis),
        point(x + 15 // dis, y - 57 // dis)
    ])
    plot([
        point(x + 12 // dis, y - 45 // dis),
        point(x + 20 // dis, y - 45 // dis)
    ])
    plot([
        point(x, y - 75 // dis),
        point(int(x - 5 / dis + .5), y - 80 // dis),
        point(x - 8 // dis, y - 75 // dis),
        point(int(x - 5 / dis + .5), y - 65 // dis),
        point(int(x + 5 / dis + .5), y - 65 // dis),
        point(int(x + 5 / dis + .5), y - 68 // dis),
        point(int(x - 5 / dis + .5), y - 68 // dis),
        point(int(x - 5 / dis + .5), y - 65 // dis),
        point(int(x + 5 / dis + .5), y - 65 // dis),
        point(int(x + 8 / dis), y - 75 // dis),
        point(int(x + 5 / dis + .5), y - 80 // dis),
        point(int(x - 5 / dis + .5), y - 80 // dis)
    ])
    plot([
        point(int(x - 5 / dis + .5), y - 72 // dis)
    ])
    plot([
        point(int(x + 5 / dis + .5), y - 72 // dis)
    ])


def _draw_thief(plot, point, x, y, dis):
    plot([
        point(x, y - 56 // dis),
        point(x, y - 8 // dis),
        point(x + 10 // dis, y),
        point(x + 30 // dis, y),
        point(x + 30 // dis, y - 45 // dis),
        point(x + 10 // dis, y - 64 // dis),
        point(x, y - 56 // dis),
        point(x - 10 // dis, y - 64 // dis),
        point(x - 30 // dis, y - 45 // dis),
        point(x - 30 // dis, y),
        point(x - 10 // dis, y),
        point(x, y - 8 // dis)
    ])
    plot([
        point(x - 10 // dis, y - 64 // dis),
        point(x - 10 // dis, y - 75 // dis),
        point(x, y - 83 // dis),
        point(x + 10 // dis, y - 75 // dis),
        point(x, y - VIEWPORT_CENTER_Y // dis),
        point(x - 10 // dis, y - 75 // dis),
        point(x, y - 60 // dis),
        point(x + 10 // dis, y - 75 // dis),
        point(x + 10 // dis, y - 64 // dis)
    ])


def _draw_giant_rat(plot, point, x, y, dis):
    plot([
        point(x + 5 // dis, y - 30 // dis),
        point(x, y - 25 // dis),
        point(x - 5 // dis, y - 30 // dis),
        point(x - 15 // dis, y - 5 // dis),
        point(x - 10 // dis, y),
        point(x + 10 // dis, y),
        point(x + 15 // dis, y - 5 // dis),
        point(x + 20 // dis, y - 5 // dis),
        point(x + 10 // dis, y),
        point(x + 15 // dis, y - 5 // dis),
        point(x + 5 // dis, y - 30 // dis),
        point(x + 10 // dis, y - 40 // dis),
        point(int(x + 3 / dis + .5), y - 35 // dis),
        point(int(x - 3 / dis + .5), y - 35 // dis),
        point(x - 10 // dis, y - 40 // dis),
        point(x - 5 // dis, y - 30 // dis)
    ])
    plot([
        point(int(x - 5 / dis), y - 33 // dis),
        point(int(x - 3 / dis + .5), y - 30 // dis)
    ])
    plot([
        point(int(x + 5 / dis), y - 33 // dis),
        point(int(x + 3 / dis + .5), y - 30 // dis)
    ])
    plot([
        point(x - 5 // dis, y - 20 // dis),
        point(x - 5 // dis, y - 15 // dis)
    ])
    plot([
        point(x + 5 // dis, y - 20 // dis),
        point(x + 5 // dis, y - 15 // dis)
    ])
    plot([
        point(x - 7 // dis, y - 20 // dis),
        point(x - 7 // dis, y - 15 // dis)
    ])
    plot([
        point(x + 7 // dis, y - 20 // dis),
        point(x + 7 // dis, y - 15 // dis)
    ])


def _draw_orc(plot, point, x, y, dis):
    plot([
        point(x, y),
        point(x - 15 // dis, y),
        point(x - 8 // dis, y - 8 // dis),
        point(x - 8 // dis, y - 15 // dis),
        point(x - 15 // dis, y - 23 // dis),
        point(x - 15 // dis, y - 15 // dis),
        point(x - 23 // dis, y - 23 // dis),
        point(x - 23 // dis, y - 45 // dis),
        point(x - 15 // dis, y - 53 // dis),
        point(x - 8 // dis, y - 53 // dis),
        point(x - 15 // dis, y - 68 // dis),
        point(x - 8 // dis, y - 75 // dis),
        point(x, y - 75 // dis)
    ])
    plot([
        point(x, y),
        point(x + 15 // dis, y),
        point(x + 8 // dis, y - 8 // dis),
        point(x + 8 // dis, y - 15 // dis),
        point(x + 15 // dis, y - 23 // dis),
        point(x + 15 // dis, y - 15 // dis),
        point(x + 23 // dis, y - 23 // dis),
        point(x + 23 // dis, y - 45 // dis),
        point(x + 15 // dis, y - 53 // dis),
        point(x + 8 // dis, y - 53 // dis),
        point(x + 15 // dis, y - 68 // dis),
        point(x + 8 // dis, y - 75 // dis),
        point(x, y - 75 // dis)
    ])
    plot([
        point(x - 15 // dis, y - 68 // dis),
        point(x + 15 // dis, y - 68 // dis)
    ])
    plot([
        point(x - 8 // dis, y - 53 // dis),
        point(x + 8 // dis, y - 53 // dis)
    ])
    plot([
        point(x - 23 // dis, y - 15 // dis),
        point(x + 8 // dis, y - 45 // dis)
    ])
    plot([
        point(x - 8 // dis, y - 68 // dis),
        point(x, y - 60 // dis),
        point(x + 8 // dis, y - 68 // dis),
        point(x + 8 // dis, y - 60 // dis),
        point(x - 8 // dis, y - 60 // dis),
        point(x - 8 // dis, y - 68 // dis)
    ])
    plot([
        point(x, y - 38 // dis),
        point(x - 8 // dis, y - 38 // dis),
        point(x + 8 // dis, y - 53 // dis),
        point(x + 8 // dis, y - 45 // dis),
        point(x + 15 // dis, y - 45 // dis),
        point(x, y - 30 // dis),
        point(x, y - 38 // dis)
    ])


def _draw_viper(plot, point, x, y, dis):
    plot([
        point(x - 10 // dis, y - 15 // dis),
        point(x - 10 // dis, y - 30 // dis),
        point(x - 15 // dis, y - 20 // dis),
        point(x - 15 // dis, y - 15 // dis),
        point(x - 15 // dis, y),
        point(x + 15 // dis, y),
        point(x + 15 // dis, y - 15 // dis),
        point(x - 15 // dis, y - 15 // dis)
    ])
    plot([
        point(x - 15 // dis, y - 10 // dis),
        point(x + 15 // dis, y - 10 // dis)
    ])
    plot([
        point(x - 15 // dis, y - 5 // dis),
        point(x + 15 // dis, y - 5 // dis)
    ])
    plot([
        point(x, y - 15 // dis),
        point(x - 5 // dis, y - 20 // dis),
        point(x - 5 // dis, y - 35 // dis),
        point(x + 5 // dis, y - 35 // dis),
        point(x + 5 // dis, y - 20 // dis),
        point(x + 10 // dis, y - 15 // dis)
    ])
    plot([
        point(x - 5 // dis, y - 20 // dis),
        point(x + 5 // dis, y - 20 // dis)
    ])
    plot([
        point(x - 5 // dis, y - 25 // dis),
        point(x + 5 // dis, y - 25 // dis)
    ])
    plot([
        point(x - 5 // dis, y - 30 // dis),
        point(x + 5 // dis, y - 30 // dis)
    ])
    plot([
        point(x - 10 // dis, y - 35 // dis),
        point(x - 10 // dis, y - 40 // dis),
        point(x - 5 // dis, y - 45 // dis),
        point(x + 5 // dis, y - 45 // dis),
        point(x + 10 // dis, y - 40 // dis),
        point(x + 10 // dis, y - 35 // dis)
    ])
    plot([
        point(x - 10 // dis, y - 40 // dis),
        point(x, y - 45 // dis),
        point(x + 10 // dis, y - 40 // dis)
    ])
    plot([
        point(x - 5 // dis, y - 40 // dis),
        point(x + 5 // dis, y - 40 // dis),
        point(x + 15 // dis, y - 30 // dis),
        point(x, y - 40 // dis),
        point(x - 15 // dis, y - 30 // dis),
        point(int(x - 5 / dis + .5), y - 40 // dis)
    ])


def _draw_carrion_crawler(plot, point, x, y, perspective):
    dis = perspective.distance
    center_y_offset = perspective.half_viewport.height
    plot([
        point(x - 20 // dis, VIEWPORT_CENTER_Y - center_y_offset),
        point(x - 20 // dis, y - 88 // dis),
        point(x - 10 // dis, y - 83 // dis),
        point(x + 10 // dis, y - 83 // dis),
        point(x + 20 // dis, y - 88 // dis),
        point(x + 20 // dis, VIEWPORT_CENTER_Y - center_y_offset),
        point(x - 20 // dis, VIEWPORT_CENTER_Y - center_y_offset)
    ])
    plot([
        point(x - 20 // dis, y - 88 // dis),
        point(x - 30 // dis, y - 83 // dis),
        point(x - 30 // dis, y - 78 // dis)
    ])
    plot([
        point(x + 20 // dis, y - 88 // dis),
        point(x + 30 // dis, y - 83 // dis),
        point(x + 40 // dis, y - 83 // dis)
    ])
    plot([
        point(x - 15 // dis, y - 86 // dis),
        point(x - 20 // dis, y - 83 // dis),
        point(x - 20 // dis, y - 78 // dis),
        point(x - 30 // dis, y - 73 // dis),
        point(x - 30 // dis, y - 68 // dis),
        point(x - 20 // dis, y - 63 // dis)
    ])
    plot([
        point(x - 10 // dis, y - 83 // dis),
        point(x - 10 // dis, y - 58 // dis),
        point(x, y - 50 // dis)
    ])
    plot([
        point(x + 10 // dis, y - 83 // dis),
        point(x + 10 // dis, y - 78 // dis),
        point(x + 20 // dis, y - 73 // dis),
        point(x + 20 // dis, y - 40 // dis)
    ])
    plot([
        point(x + 15 // dis, y - 85 // dis),
        point(x + 20 // dis, y - 78 // dis),
        point(x + 30 // dis, y - 76 // dis),
        point(x + 30 // dis, y - 60 // dis)
    ])
    plot([
        point(x, y - 83 // dis),
        point(x, y - 73 // dis),
        point(x + 10 // dis, y - 68 // dis),
        point(x + 10 // dis, y - 63 // dis),
        point(x, y - 58 // dis)
    ])


def _draw_gremlin(plot, point, x, y, dis):
    plot([
        point(int(x + 5 / dis + .5), y - 10 // dis),
        point(int(x - 5 / dis + .5), y - 10 // dis),
        point(int(x), y - 15 // dis),
        point(int(x + 10 / dis), y - 20 // dis),
        point(int(x + 5 / dis + .5), y - 15 // dis),
        point(int(x + 5 / dis + .5), y - 10 // dis),
        point(int(x + 7 / dis + .5), y - 6 // dis),
        point(int(x + 5 / dis + .5), y - 3 // dis),
        point(int(x - 5 / dis + .5), y - 3 // dis),
        point(int(x - 7 / dis + .5), y - 6 // dis),
        point(int(x - 5 / dis + .5), y - 10 // dis)
    ])
    plot([
        point(int(x + 2 / dis + .5), y - 3 // dis),
        point(int(x + 5 / dis + .5), y),
        point(int(x + 8 / dis), y)
    ])
    plot([
        point(int(x - 2 / dis + .5), y - 3 // dis),
        point(int(x - 5 / dis + .5), y),
        point(int(x - 8 / dis), y)
    ])
    plot([
        point(int(x + 3 / dis + .5), y - 8 // dis)
    ])
    plot([
        point(int(x - 3 / dis + .5), y - 8 // dis)
    ])
    plot([
        point(int(x + 3 / dis + .5), y - 5 // dis),
        point(int(x - 3 / dis + .5), y - 5 // dis)
    ])


# noinspection PyUnusedLocal
def _draw_mimic(plot, point, x, y, perspective):
    dis = perspective.distance
    plot([
        point(VIEWPORT_CENTER_X - 10 // dis, perspective.bottom),
        point(VIEWPORT_CENTER_X - 10 // dis, perspective.bottom - 10 // dis),
        point(VIEWPORT_CENTER_X + 10 // dis, perspective.bottom - 10 // dis),
        point(VIEWPORT_CENTER_X + 10 // dis, perspective.bottom),
        point(VIEWPORT_CENTER_X - 10 // dis, perspective.bottom)
    ])
    plot([
        point(VIEWPORT_CENTER_X - 10 // dis, perspective.bottom - 10 // dis),
        point(VIEWPORT_CENTER_X - 5 // dis, perspective.bottom - 15 // dis),
        point(VIEWPORT_CENTER_X + 15 // dis, perspective.bottom - 15 // dis),
        point(VIEWPORT_CENTER_X + 15 // dis, perspective.bottom - 5 // dis),
        point(VIEWPORT_CENTER_X + 10 // dis, perspective.bottom)
    ])
    plot([
        point(VIEWPORT_CENTER_X + 10 // dis, perspective.bottom - 10 // dis),
        point(VIEWPORT_CENTER_X + 15 // dis, perspective.bottom - 15 // dis)
    ])


def _draw_daemon(plot, point, x, y, dis):
    plot([
        point(x - 14 // dis, y - 46 // dis),
        point(x - 12 // dis, y - 37 // dis),
        point(x - 20 // dis, y - 32 // dis),
        point(x - 30 // dis, y - 32 // dis),
        point(x - 22 // dis, y - 24 // dis),
        point(x - 40 // dis, y - 17 // dis),
        point(x - 40 // dis, y - 7 // dis),
        point(x - 38 // dis, y - 5 // dis),
        point(x - 40 // dis, y - 3 // dis),
        point(x - 40 // dis, y),
        point(x - 36 // dis, y),
        point(x - 34 // dis, y - 2 // dis),
        point(x - 32 // dis, y),
        point(x - 28 // dis, y),
        point(x - 28 // dis, y - 3 // dis),
        point(x - 30 // dis, y - 5 // dis),
        point(x - 28 // dis, y - 7 // dis),
        point(x - 28 // dis, y - 15 // dis),
        point(x, y - 27 // dis)
    ])
    plot([
        point(x + 14 // dis, y - 46 // dis),
        point(x + 12 // dis, y - 37 // dis),
        point(x + 20 // dis, y - 32 // dis),
        point(x + 30 // dis, y - 32 // dis),
        point(x + 22 // dis, y - 24 // dis),
        point(x + 40 // dis, y - 17 // dis),
        point(x + 40 // dis, y - 7 // dis),
        point(x + 38 // dis, y - 5 // dis),
        point(x + 40 // dis, y - 3 // dis),
        point(x + 40 // dis, y),
        point(x + 36 // dis, y),
        point(x + 34 // dis, y - 2 // dis),
        point(x + 32 // dis, y),
        point(x + 28 // dis, y),
        point(x + 28 // dis, y - 3 // dis),
        point(x + 30 // dis, y - 5 // dis),
        point(x + 28 // dis, y - 7 // dis),
        point(x + 28 // dis, y - 15 // dis),
        point(x, y - 27 // dis)
    ])
    plot([
        point(x + 6 // dis, y - 48 // dis),
        point(x + 38 // dis, y - 41 // dis),
        point(x + 40 // dis, y - 42 // dis),
        point(x + 18 // dis, y - 56 // dis),
        point(x + 12 // dis, y - 56 // dis),
        point(x + 10 // dis, y - 57 // dis),
        point(x + 8 // dis, y - 56 // dis),
        point(x - 8 // dis, y - 56 // dis),
        point(x - 10 // dis, y - 58 // dis),
        point(x + 14 // dis, y - 58 // dis),
        point(x + 16 // dis, y - 59 // dis),
        point(x + 8 // dis, y - 63 // dis),
        point(x + 6 // dis, y - 63 // dis),
        point(int(x + 2 / dis + .5), y - 70 // dis),
        point(int(x + 2 / dis + .5), y - 63 // dis),
        point(int(x - 2 / dis + .5), y - 63 // dis),
        point(int(x - 2 / dis + .5), y - 70 // dis),
        point(x - 6 // dis, y - 63 // dis),
        point(x - 8 // dis, y - 63 // dis),
        point(x - 16 // dis, y - 59 // dis),
        point(x - 14 // dis, y - 58 // dis),
        point(x - 10 // dis, y - 57 // dis),
        point(x - 12 // dis, y - 56 // dis),
        point(x - 18 // dis, y - 56 // dis),
        point(x - 36 // dis, y - 47 // dis),
        point(x - 36 // dis, y - 39 // dis),
        point(x - 28 // dis, y - 41 // dis),
        point(x - 28 // dis, y - 46 // dis),
        point(x - 20 // dis, y - 50 // dis),
        point(x - 18 // dis, y - 50 // dis),
        point(x - 14 // dis, y - 46 // dis)
    ])
    plot([
        point(x - 28 // dis, y - 41 // dis),
        point(x + 30 // dis, y - 55 // dis),
    ])
    plot([
        point(x + 28 // dis, y - 58 // dis),
        point(x + 22 // dis, y - 56 // dis),
        point(x + 22 // dis, y - 53 // dis),
        point(x + 28 // dis, y - 52 // dis),
        point(x + 34 // dis, y - 54 // dis)
    ])
    plot([
        point(x + 20 // dis, y - 50 // dis),
        point(x + 26 // dis, y - 47 // dis)
    ])
    plot([
        point(x + 10 // dis, y - 58 // dis),
        point(x + 10 // dis, y - 61 // dis),
        point(x + 4 // dis, y - 58 // dis)
    ])
    plot([
        point(x - 10 // dis, y - 58 // dis),
        point(x - 10 // dis, y - 61 // dis),
        point(x - 4 // dis, y - 58 // dis)
    ])
    plot([
        point(x + 40 // dis, y - 9 // dis),
        point(x + 50 // dis, y - 12 // dis),
        point(x + 40 // dis, y - 7 // dis)
    ])
    plot([
        point(x - 8 // dis, y - 25 // dis),
        point(x + 6 // dis, y - 7 // dis),
        point(x + 28 // dis, y - 7 // dis),
        point(x + 28 // dis, y - 9 // dis),
        point(x + 20 // dis, y - 9 // dis),
        point(x + 6 // dis, y - 25 // dis)
    ])


def _draw_balrog(plot, point, x, y, dis):
    plot([
        point(x + 6 // dis, y - 60 // dis),
        point(x + 30 // dis, y - 90 // dis),
        point(x + 60 // dis, y - 30 // dis),
        point(x + 60 // dis, y - 10 // dis),
        point(x + 30 // dis, y - 40 // dis),
        point(x + 15 // dis, y - 40 // dis)
    ])
    plot([
        point(x - 6 // dis, y - 60 // dis),
        point(x - 30 // dis, y - 90 // dis),
        point(x - 60 // dis, y - 30 // dis),
        point(x - 60 // dis, y - 10 // dis),
        point(x - 30 // dis, y - 40 // dis),
        point(x - 15 // dis, y - 40 // dis)
    ])
    plot([
        point(x, y - 25 // dis),
        point(x + 6 // dis, y - 25 // dis),
        point(x + 10 // dis, y - 20 // dis),
        point(x + 12 // dis, y - 10 // dis),
        point(x + 10 // dis, y - 6 // dis),
        point(x + 10 // dis, y),
        point(x + 14 // dis, y),
        point(x + 15 // dis, y - 5 // dis),
        point(x + 16 // dis, y),
        point(x + 20 // dis, y),
        point(x + 20 // dis, y - 6 // dis),
        point(x + 18 // dis, y - 10 // dis),
        point(x + 18 // dis, y - 20 // dis),
        point(x + 15 // dis, y - 30 // dis),
        point(x + 15 // dis, y - 45 // dis),
        point(x + 40 // dis, y - 60 // dis),
        point(x + 40 // dis, y - 70 // dis),
        point(x + 10 // dis, y - 55 // dis),
        point(x + 6 // dis, y - 60 // dis),
        point(x + 10 // dis, y - 74 // dis),
        point(x + 6 // dis, y - 80 // dis),
        point(int(x + 4 / dis + .5), y - 80 // dis),
        point(int(x + 3 / dis + .5), y - 82 // dis),
        point(int(x + 2 / dis + .5), y - 80 // dis),
        point(x, y - 80 // dis)
    ])
    plot([
        point(x, y - 25 // dis),
        point(x - 6 // dis, y - 25 // dis),
        point(x - 10 // dis, y - 20 // dis),
        point(x - 12 // dis, y - 10 // dis),
        point(x - 10 // dis, y - 6 // dis),
        point(x - 10 // dis, y),
        point(x - 14 // dis, y),
        point(x - 15 // dis, y - 5 // dis),
        point(x - 16 // dis, y),
        point(x - 20 // dis, y),
        point(x - 20 // dis, y - 6 // dis),
        point(x - 18 // dis, y - 10 // dis),
        point(x - 18 // dis, y - 20 // dis),
        point(x - 15 // dis, y - 30 // dis),
        point(x - 15 // dis, y - 45 // dis),
        point(x - 40 // dis, y - 60 // dis),
        point(x - 40 // dis, y - 70 // dis),
        point(x - 10 // dis, y - 55 // dis),
        point(x - 6 // dis, y - 60 // dis),
        point(x - 10 // dis, y - 74 // dis),
        point(x - 6 // dis, y - 80 // dis),
        point(int(x - 4 / dis + .5), y - 80 // dis),
        point(int(x - 3 / dis + .5), y - 82 // dis),
        point(int(x - 2 / dis + .5), y - 80 // dis),
        point(x, y - 80 // dis)
    ])
    plot([
        point(x - 6 // dis, y - 25 // dis),
        point(x, y - 6 // dis),
        point(x + 10 // dis, y),
        point(int(x + 4 / dis + .5), y - 8 // dis),
        point(x + 6 // dis, y - 25 // dis)
    ])
    plot([
        point(x - 40 // dis, y - 64 // dis),
        point(x - 40 // dis, y - 90 // dis),
        point(x - 52 // dis, y - 80 // dis),
        point(x - 52 // dis, y - 40 // dis)
    ])
    plot([
        point(x + 40 // dis, y - 86 // dis),
        point(x + 38 // dis, y - 92 // dis),
        point(x + 42 // dis, y - 92 // dis),
        point(x + 40 // dis, y - 86 // dis),
        point(x + 40 // dis, y - 50 // dis)
    ])
    plot([
        point(int(x + 4 / dis + .5), y - 70 // dis),
        point(int(x + 6 / dis), y - 74 // dis)
    ])
    plot([
        point(int(x - 4 / dis + .5), y - 70 // dis),
        point(int(x - 6 / dis), y - 74 // dis)
    ])
    plot([
        point(int(x), y - 64 // dis),
        point(int(x), y - 60 // dis)
    ])
