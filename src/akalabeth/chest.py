from constants import *


def draw_chest_front_side(plot, point, perspective):
    dis = perspective.distance
    plot([
        point(VIEWPORT_CENTER_X - 10 // dis, perspective.bottom),
        point(VIEWPORT_CENTER_X - 10 // dis, perspective.bottom - 10 // dis),
        point(VIEWPORT_CENTER_X + 10 // dis, perspective.bottom - 10 // dis),
        point(VIEWPORT_CENTER_X + 10 // dis, perspective.bottom),
        point(VIEWPORT_CENTER_X - 10 // dis, perspective.bottom)
    ])


def draw_chest(plot, point, perspective):
    dis = perspective.distance
    plot([
        point(VIEWPORT_CENTER_X - 10 // dis, perspective.bottom - 10 // dis),
        point(VIEWPORT_CENTER_X - 5 // dis, perspective.bottom - 15 // dis),
        point(VIEWPORT_CENTER_X + 15 // dis, perspective.bottom - 15 // dis),
        point(VIEWPORT_CENTER_X + 15 // dis, perspective.bottom - 5 // dis),
        point(VIEWPORT_CENTER_X + 10 // dis, perspective.bottom)
    ])
    plot([
        point(VIEWPORT_CENTER_X + 10 // dis, perspective.bottom - 10 // dis),
        point(VIEWPORT_CENTER_X + 15 // dis, perspective.bottom - 15 // dis)
    ])

