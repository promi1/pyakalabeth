from castle import Castle
from stats import Stats
from shop import Shop
from monsters import Monsters
from fight import Fight
from world_object import WorldObject
from world_renderer import WorldRenderer
from dungeon_object import DungeonObject
from dungeon_generator import DungeonGenerator


class Turn:
    def __init__(self, bas, main, dungeon, dungeon_renderer):
        self.bas = bas
        self.main = main
        self.dungeon = dungeon
        self.dungeon_renderer = dungeon_renderer

    def render_map(self):
        # 100 - 190
        renderer = WorldRenderer(self.bas, self.main.world, self.main.world_player_pos_x, self.main.world_player_pos_y)
        renderer.run()

    def render_dungeon(self):
        # 200 - 491
        self.dungeon_renderer.run(self.main.dungeon_player_pos_x, self.main.dungeon_player_pos_y,
                                  self.main.dungeon_player_dir_x, self.main.dungeon_player_dir_y)

    def randomize_dungeon(self):
        # 500
        generator = DungeonGenerator(self.bas, self.dungeon, self.main.difficulty)
        generator.run(self.main.dungeon_player_pos_x, self.main.dungeon_player_pos_y)

    def run(self):
        self.render_map()
        game_over = False
        while not game_over:
            action_taken = True
            action_key = self.read_player_command()
            self.bas.poke(-16368, 0)
            inout_sgn_plus_1 = self.bas.sgn(self.dungeon.level) + 1
            # CR = North / Forward
            if action_key == '↑':
                if inout_sgn_plus_1 == 1:
                    self.go_north()
                elif inout_sgn_plus_1 == 2:
                    self.go_forward()
            # RIGHT ARROW
            elif action_key == '→':
                if inout_sgn_plus_1 == 1:
                    self.go_east()
                elif inout_sgn_plus_1 == 2:
                    self.turn_right()
            # LEFT ARROW
            elif action_key == '←':
                if inout_sgn_plus_1 == 1:
                    self.go_west()
                elif inout_sgn_plus_1 == 2:
                    self.turn_left()
            # / = South / turn around
            elif action_key == '↓':
                if inout_sgn_plus_1 == 1:
                    self.go_south()
                elif inout_sgn_plus_1 == 2:
                    self.turn_around()
            # X = Go
            elif action_key == 'X':
                if inout_sgn_plus_1 == 1:
                    if not self.enter_shop_dungeon_castle():
                        action_taken = False
                elif inout_sgn_plus_1 == 2:
                    self.go_dungeon_floor()
            # A = Attack
            elif action_key == 'A':
                if inout_sgn_plus_1 == 1:
                    self.pass_turn()
                elif inout_sgn_plus_1 == 2:
                    self.attack()
            # SPACE = Pass
            elif action_key == ' ':
                self.bas.print('PASS')
            # S = Show stats / inventory
            elif action_key == 'S':
                self.print_stats_and_wait_for_cr()
            # P = Toggle pause
            elif action_key == 'P':
                if self.main.pause_after_action:
                    self.main.pause_after_action = False
                    self.bas.print('PAUSE OFF')
                else:
                    self.main.pause_after_action = True
                    self.bas.print('PAUSE ON')
                action_taken = False
            else:
                self.bas.print('HUH?')
                action_taken = False
            if action_taken:
                game_over = not self.react_to_player_action()

    def react_to_player_action(self):
        # 1090
        self.main.player_items[0] = self.main.player_items[0] - 1 + self.bas.sgn(self.dungeon.level) * .9
        if self.main.player_items[0] < 0:
            self.main.character_attribute_values[0] = 0
            self.print_starved()
        else:
            self.print_food_hp_gold()
            self.main.player_items[0] = self.bas.int(self.main.player_items[0] * 10) / 10
        if self.main.character_attribute_values[0] <= 0:
            return False
        # In a dungeon?
        if self.dungeon.level > 0:
            self.run_monsters()
            if self.main.character_attribute_values[0] <= 0:
                return False
        self.print_food_hp_gold()
        if self.dungeon.level == 0:
            self.render_map()
        else:
            self.render_dungeon()
        return True

    def print_starved(self):
        self.bas.print()
        self.bas.print('YOU HAVE STARVED!!!!!')

    def go_north(self):
        self.bas.print('NORTH')
        if self._get_world_object(0, -1) == WorldObject.MOUNTAIN:
            self.bas.print("YOU CAN'T PASS THE MOUNTAINS")
            return
        self.main.world_player_pos_y -= 1

    def go_east(self):
        self.bas.print('EAST')
        if self._get_world_object(1, 0) == WorldObject.MOUNTAIN:
            self.bas.print("YOU CAN'T PASS THE MOUNTAINS")
            return
        self.main.world_player_pos_x += 1

    def go_west(self):
        self.bas.print('WEST')
        if self._get_world_object(-1, 0) == WorldObject.MOUNTAIN:
            self.bas.print("YOU CAN'T PASS THE MOUNTAINS")
            return
        self.main.world_player_pos_x -= 1

    def go_south(self):
        self.bas.print('SOUTH')
        if self._get_world_object(0, 1) == WorldObject.MOUNTAIN:
            self.bas.print("YOU CAN'T PASS THE MOUNTAINS")
            return
        self.main.world_player_pos_y += 1

    def go_forward(self):
        destination_x = self.main.dungeon_player_pos_x + self.main.dungeon_player_dir_x
        destination_y = self.main.dungeon_player_pos_y + self.main.dungeon_player_dir_y
        tmp = self.main.dungeon.map[destination_x][destination_y]
        if tmp.obj != DungeonObject.WALL and tmp.monster == 0:
            self.main.dungeon_player_pos_x += self.main.dungeon_player_dir_x
            self.main.dungeon_player_pos_y += self.main.dungeon_player_dir_y
        self.bas.print('FORWARD')
        if tmp.obj == DungeonObject.TRAP:
            self.bas.print('AAARRRGGGHHH!!! A TRAP!')
            self.main.character_attribute_values[0] -= self.bas.int(self.bas.rnd(1) * self.dungeon.level + 3)
            self.dungeon.level += 1
            self.bas.print(f'FALLING TO LEVEL {self.dungeon.level}')
            self.randomize_dungeon()
            return
        if tmp.obj == DungeonObject.CHEST:
            self.dungeon.map[self.main.dungeon_player_pos_x][self.main.dungeon_player_pos_y].obj = DungeonObject.EMPTY0
            self.bas.print('GOLD!!!!!')
            z = self.bas.int(self.bas.rnd(1) * 5 * self.dungeon.level + self.dungeon.level)
            self.bas.print(f'{z}-PIECES OF EIGHT')
            self.main.character_attribute_values[5] += z
            if z > 0:
                z = self.bas.int(self.bas.rnd(1) * 6)
                self.bas.print(f'AND A {self.main.item_names[z]}')
                self.main.player_items[z] += 1

    def turn_right(self):
        self.bas.print('TURN RIGHT')
        if self.main.dungeon_player_dir_x != 0:
            self.main.dungeon_player_dir_y = self.main.dungeon_player_dir_x
            self.main.dungeon_player_dir_x = 0
            return
        self.main.dungeon_player_dir_x = -self.main.dungeon_player_dir_y
        self.main.dungeon_player_dir_y = 0

    def turn_left(self):
        self.bas.print('TURN LEFT')
        if self.main.dungeon_player_dir_x != 0:
            self.main.dungeon_player_dir_y = -self.main.dungeon_player_dir_x
            self.main.dungeon_player_dir_x = 0
            return
        self.main.dungeon_player_dir_x = self.main.dungeon_player_dir_y
        self.main.dungeon_player_dir_y = 0

    def turn_around(self):
        self.bas.print('TURN AROUND')
        self.main.dungeon_player_dir_x = -self.main.dungeon_player_dir_x
        self.main.dungeon_player_dir_y = -self.main.dungeon_player_dir_y

    def enter_shop_dungeon_castle(self):
        world_object = self.main.world[self.main.world_player_pos_x][self.main.world_player_pos_y]
        if world_object == WorldObject.TOWN:
            self.show_stats()
            self.enter_shop()
            return True
        if world_object == WorldObject.DUNGEON and self.dungeon.level == 0:
            self.bas.print('GO DUNGEON')
            self.bas.print('PLEASE WAIT ')
            self.dungeon.level = 1
            self.randomize_dungeon()
            self.main.dungeon_player_dir_x = 1
            self.main.dungeon_player_dir_y = 0
            self.main.dungeon_player_pos_x = 1
            self.main.dungeon_player_pos_y = 1
            return True
        if world_object == WorldObject.CASTLE:
            self.enter_castle()
            return True
        self.bas.print('HUH?')
        return False

    def go_dungeon_floor(self):
        tile = self.dungeon.map[self.main.dungeon_player_pos_x][self.main.dungeon_player_pos_y].obj
        match tile:
            case DungeonObject.DOWN_LADDER:
                self.go_down()
            case DungeonObject.UP_LADDER:
                self.go_up()
            case DungeonObject.DOWN_HOLE:
                self.go_down()
            case _:
                self.bas.print('HUH?')

    def go_down(self):
        self.bas.print(f'GO DOWN TO LEVEL {self.dungeon.level + 1}')
        self.dungeon.level += 1
        self.randomize_dungeon()

    def go_up(self):
        if self.dungeon.level == 1:
            self.bas.print('LEAVE DUNGEON')
            self.dungeon.level = 0
            self.add_hit_points_after_dungeon()
        else:
            self.bas.print(f'GO UP TO LEVEL {self.dungeon.level - 1}')
            self.dungeon.level -= 1
            self.randomize_dungeon()

    def add_hit_points_after_dungeon(self):
        self.bas.print('THOU HAST GAINED')
        self.bas.print(f'{self.main.dungeon_bonus_hit_points} HIT POINTS')
        self.main.character_attribute_values[0] += self.main.dungeon_bonus_hit_points
        self.main.dungeon_bonus_hit_points = 0

    def pass_turn(self):
        return

    def run_monsters(self):
        monsters = Monsters(self.bas, self.main, self.dungeon)
        monsters.run()

    def attack(self):
        # 1650
        fight = Fight(self.bas, self.main, self.dungeon)
        fight.run()

    def print_stats_and_wait_for_cr(self):
        self.show_stats()
        self.bas.home()
        self.bas.print_no_newline('PRESS -CR- TO CONTINUE')
        self.bas.input()
        self.bas.text()
        self.bas.home()

    def read_player_command(self):
        self.bas.vtab(24)
        self.bas.print_no_newline('COMMAND? ')
        self.bas.call(-868)
        x = None
        while not x:
            x = self.bas.peek(-16384)
        return x

    def print_food_hp_gold(self):
        self.bas.poke(33, 40)
        self.print_food()
        self.print_hp()
        self.print_gold()
        self.bas.poke(33, 29)
        self.bas.htab(1)

    def print_food(self):
        self.bas.vtab(22)
        self.bas.htab(30)
        self.bas.print_no_newline(f'FOOD={int(self.main.player_items[0])}')
        self.bas.call(-868)

    def print_hp(self):
        self.bas.vtab(23)
        self.bas.htab(30)
        self.bas.print_no_newline(f'H.P.={self.main.character_attribute_values[0]}')
        self.bas.call(-868)

    def print_gold(self):
        self.bas.vtab(24)
        self.bas.htab(30)
        self.bas.print_no_newline(f'GOLD={self.main.character_attribute_values[5]}')
        self.bas.call(-868)

    def enter_castle(self):
        # 7000 - 7990
        castle = Castle(self.bas, self.main)
        castle.run()

    def show_stats(self):
        stats = Stats(self.bas, self.main)
        stats.run()

    def enter_shop(self):
        shop = Shop(self.bas, self.main)
        shop.run()

    def _get_world_object(self, x_offset, y_offset):
        return self.main.world[self.main.world_player_pos_x + x_offset][self.main.world_player_pos_y + y_offset]
