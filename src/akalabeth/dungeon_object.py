from enum import Enum


class DungeonObject(Enum):
    EMPTY0 = 0
    WALL = 1
    TRAP = 2
    HIDDEN_DOOR = 3
    NORMAL_DOOR = 4
    CHEST = 5
    EMPTY6 = 6
    DOWN_LADDER = 7
    UP_LADDER = 8
    DOWN_HOLE = 9
