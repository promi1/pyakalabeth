# noinspection PyMethodMayBeStatic
class Castle:
    def __init__(self, bas, main):
        self.bas = bas
        self.main = main

    def run(self):
        game_finished = False
        self.print_castle_prefix()
        if self.main.player_name == '':
            self.print_castle_intro()
            self.input_player_name()
            if not self.confirm_adventure():
                return
            self.set_first_task()
            self.print_task()
        else:
            if self.main.quest_monster > 0:
                self.print_task_unfulfilled()
                return
            self.print_task_accomplished()
            if self.bas.abs(self.main.quest_monster) == 10:
                game_finished = True
            else:
                self.set_next_task()
                self.print_new_task()
            if game_finished:
                self.promote_and_print_finished_message()
        self.exit_castle_after_completed_task(game_finished)

    def print_castle_prefix(self):
        self.bas.home()
        self.bas.text()
        self.bas.home()
        self.bas.call(62450)

    def print_castle_intro(self):
        self.bas.print()
        self.bas.print()
        self.bas.print('     WELCOME PEASANT INTO THE HALLS OF')
        self.bas.print('THE MIGHTY LORD BRITISH. HEREIN THOU MAY CHOOSE TO DARE BATTLE WITH THE EVIL')
        self.bas.print('CREATURES OF THE DEPTHS, FOR GREAT')
        self.bas.print('REWARD!')
        self.bas.print()

    def input_player_name(self):
        self.bas.print_no_newline('WHAT IS THY NAME PEASANT ')
        self.main.player_name = self.bas.input()

    def confirm_adventure(self):
        self.bas.print_no_newline('DOEST THOU WISH FOR GRAND ADVENTURE ? ')
        player_input = self.bas.get()
        if player_input == 'Y':
            return True
        self.bas.print()
        self.bas.print('THEN LEAVE AND BEGONE!')
        self.main.player_name = ''
        self.continue_after_space_with_clear()
        return False

    def print_task(self):
        self.bas.print()
        self.bas.print()
        self.bas.print('GOOD! THOU SHALT TRY TO BECOME A ')
        self.bas.print('KNIGHT!!!')
        self.bas.print()
        self.bas.print('THY FIRST TASK IS TO GO INTO THE')
        self.bas.print('DUNGEONS AND TO RETURN ONLY AFTER')
        self.bas.print_no_newline(f'KILLING A(N) {self.main.monster_names[self.main.quest_monster - 1]}')

    def exit_castle_after_completed_task(self, game_finished):
        if not game_finished:
            self.bas.print()
            self.bas.print('     GO NOW UPON THIS QUEST, AND MAY')
            self.bas.print('LADY LUCK BE FAIR UNTO YOU.....')
            self.bas.print('.....ALSO I, BRITISH, HAVE INCREASED')
            self.bas.print('EACH OF THY ATTRIBUTES BY ONE!')
        self.increase_all_player_attributes()
        self.continue_after_space_with_clear()

    def print_task_unfulfilled(self):
        self.bas.print()
        self.bas.print()
        self.bas.print(f'{self.main.player_name} WHY HAST THOU RETURNED?')
        self.bas.print(f'THOU MUST KILL A(N) {self.main.monster_names[self.main.quest_monster - 1]}')
        self.bas.print('GO NOW AND COMPLETE THY QUEST!')
        self.continue_after_space_with_clear()

    def print_task_accomplished(self):
        self.bas.print()
        self.bas.print()
        self.bas.print()
        self.bas.print(f'AAHH!!.....{self.main.player_name}')
        self.bas.print()
        self.bas.print('THOU HAST ACCOMPLISHED THY QUEST!')

    def print_new_task(self):
        self.bas.print('UNFORTUNATELY, THIS IS NOT ENOUGH TO')
        self.bas.print('BECOME A KNIGHT.')
        self.bas.print()
        self.bas.print(f'NOW THOU MUST KILL A(N) {self.main.monster_names[self.main.quest_monster - 1]}')

    def promote_and_print_finished_message(self):
        self.bas.text()
        self.bas.home()
        self.bas.print()
        self.bas.print()
        self.bas.print()
        self.bas.player_name = f'LORD {self.main.player_name}'
        self.bas.print(f' {self.main.player_name},')
        self.bas.print('       THOU HAST PROVED THYSELF WORTHY')
        self.bas.print('OF KNIGHTHOOD, CONTINUE PLAY IF THOU')
        self.bas.print('DOTH WISH, BUT THOU HAST ACCOMPLISHED')
        self.bas.print('THE MAIN OBJECTIVE OF THIS GAME...')
        if self.main.difficulty == 10:
            self.bas.print()
            self.bas.print('...CALL CALIFORNIA PACIFIC COMPUTER')
            self.bas.print('AT (415)-569-9126 TO REPORT THIS')
            self.bas.print('AMAZING FEAT!')
        else:
            self.bas.print()
            self.bas.print('   NOW MAYBE THOU ART FOOLHARDY')
            self.bas.print(f'ENOUGH TO TRY DIFFICULTY LEVEL {self.main.difficulty + 1}')

    def continue_after_space_with_clear(self):
        self.bas.print()
        self.bas.print_no_newline('         PRESS -SPACE- TO CONT.')
        self.bas.get()
        self.bas.home()

    def increase_all_player_attributes(self):
        for x in range(0, 6):
            self.main.character_attribute_values[x] = self.main.character_attribute_values[x] + 1

    def set_next_task(self):
        self.main.quest_monster = self.bas.abs(self.main.quest_monster) + 1

    def set_first_task(self):
        self.main.quest_monster = self.bas.int(self.main.character_attribute_values[4] / 3)
        if self.main.quest_monster > 10:
            self.main.quest_monster = 10
        if self.main.quest_monster < 1:
            self.main.quest_monster = 1
