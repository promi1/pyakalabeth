from constants import *


class CenterDoorComponents:
    def __init__(self, perspective):
        assert perspective
        center_offset_x, center_offset_y = perspective.half_viewport.width, perspective.half_viewport.height
        self.left = VIEWPORT_CENTER_X - center_offset_x // 3
        self.right = VIEWPORT_CENTER_X + center_offset_x // 3
        self.top = int(VIEWPORT_CENTER_Y - center_offset_y * .7)
        self.bottom = VIEWPORT_CENTER_Y + center_offset_y


class SideDoorComponents:
    def __init__(self, perspective_1, perspective_2):
        assert perspective_1
        assert perspective_2
        self.near = (perspective_1.left * 2 + perspective_2.left) // 3
        self.far = (perspective_1.left + 2 * perspective_2.left) // 3
        w = self.near - perspective_1.left
        self.near_top = int(perspective_1.top + w * VIEWPORT_ASPECT_RATIO)
        self.far_top = int(perspective_1.top + 2 * w * VIEWPORT_ASPECT_RATIO)
        self.near_bottom = (perspective_1.bottom * 2 + perspective_2.bottom) // 3
        self.far_bottom = (perspective_1.bottom + 2 * perspective_2.bottom) // 3
        self.near_top = int(self.near_bottom - (self.near_bottom - self.near_top) * .8)
        self.far_top = int(self.far_bottom - (self.far_bottom - self.far_top) * .8)
        if self.far_top == self.near_bottom:
            self.far_top = self.far_top - 1


def draw_center_door(plot, point, components):
    plot([
        point(components.left, components.bottom),
        point(components.left, components.top),
        point(components.right, components.top),
        point(components.right, components.bottom)
    ])


def draw_left_door_far(plot, point, components):
    plot([
        point(components.near, components.near_bottom),
        point(components.near, components.near_top),
        point(components.far, components.far_top),
        point(components.far, components.far_bottom)
    ])


def draw_left_door_near(plot, point, components):
    plot([
        point(0, components.near_top - 3),
        point(components.far, components.far_top),
        point(components.far, components.far_bottom)
    ])


def draw_right_door_far(plot, point, components):
    plot([
        point(VIEWPORT_MAX_X - components.near, components.near_bottom),
        point(VIEWPORT_MAX_X - components.near, components.near_top),
        point(VIEWPORT_MAX_X - components.far, components.far_top),
        point(VIEWPORT_MAX_X - components.far, components.far_bottom)
    ])


def draw_right_door_near(plot, point, components):
    plot([
        point(VIEWPORT_MAX_X, components.near_top - 3),
        point(VIEWPORT_MAX_X - components.far, components.far_top),
        point(VIEWPORT_MAX_X - components.far, components.far_bottom)
    ])
