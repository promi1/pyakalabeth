from world_object import WorldObject


class WorldRenderer:
    def __init__(self, bas, world, player_x, player_y):
        self.bas = bas
        self.world = world
        self.player_x = player_x
        self.player_y = player_y

    def run(self):
        self.bas.hgr()
        self._render_player_position()
        self._render_objects()

    def _render_objects(self):
        for y in range(-1, 2):
            for x in range(-1, 2):
                kind = self.world[self.player_x + x][self.player_y + y]
                screen_x = 65 + (x + 1) * 50
                screen_y = (y + 1) * 50
                self._render_object(screen_x, screen_y, kind)

    def _render_player_position(self):
        self.bas.hplot([self.bas.point(138, 75), self.bas.point(142, 75)])
        self.bas.hplot([self.bas.point(140, 73), self.bas.point(140, 77)])

    def _render_object(self, x, y, kind):
        match kind:
            case WorldObject.MOUNTAIN:
                self._render_mountain(x, y)
            case WorldObject.FIELD:
                self._render_field(x, y)
            case WorldObject.TOWN:
                self._render_town(x, y)
            case WorldObject.DUNGEON:
                self._render_dungeon(x, y)
            case WorldObject.CASTLE:
                self._render_castle(x, y)

    def _render_mountain(self, x, y):
        self.bas.hplot([
            self.bas.point(x + 10, y + 50),
            self.bas.point(x + 10, y + 40),
            self.bas.point(x + 20, y + 30),
            self.bas.point(x + 40, y + 30),
            self.bas.point(x + 40, y + 50)
        ])
        self.bas.hplot([
            self.bas.point(x, y + 10),
            self.bas.point(x + 10, y + 10)
        ])
        self.bas.hplot([
            self.bas.point(x + 50, y + 10),
            self.bas.point(x + 40, y + 10)
        ])
        self.bas.hplot([
            self.bas.point(x, y + 40),
            self.bas.point(x + 10, y + 40)
        ])
        self.bas.hplot([
            self.bas.point(x + 40, y + 40),
            self.bas.point(x + 50, y + 40)
        ])
        self.bas.hplot([
            self.bas.point(x + 10, y),
            self.bas.point(x + 10, y + 20),
            self.bas.point(x + 20, y + 20),
            self.bas.point(x + 20, y + 30),
            self.bas.point(x + 30, y + 30),
            self.bas.point(x + 30, y + 10),
            self.bas.point(x + 40, y + 10),
            self.bas.point(x + 40, y)
        ])

    def _render_field(self, x, y):
        self.bas.hplot([
            self.bas.point(x + 20, y + 20),
            self.bas.point(x + 30, y + 20),
            self.bas.point(x + 30, y + 30),
            self.bas.point(x + 20, y + 30),
            self.bas.point(x + 20, y + 20)
        ])

    def _render_town(self, x, y):
        self.bas.hplot([
            self.bas.point(x + 10, y + 10),
            self.bas.point(x + 20, y + 10),
            self.bas.point(x + 20, y + 40),
            self.bas.point(x + 10, y + 40),
            self.bas.point(x + 10, y + 30),
            self.bas.point(x + 40, y + 30),
            self.bas.point(x + 40, y + 40),
            self.bas.point(x + 30, y + 40),
            self.bas.point(x + 30, y + 10),
            self.bas.point(x + 40, y + 10),
            self.bas.point(x + 40, y + 20),
            self.bas.point(x + 10, y + 20),
            self.bas.point(x + 10, y + 10)
        ])

    def _render_dungeon(self, x, y):
        self.bas.hplot([
            self.bas.point(x + 20, y + 20),
            self.bas.point(x + 30, y + 30)
        ])
        self.bas.hplot([
            self.bas.point(x + 20, y + 30),
            self.bas.point(x + 30, y + 20)
        ])

    def _render_castle(self, x, y):
        self.bas.hplot([
            self.bas.point(x, y),
            self.bas.point(x + 50, y),
            self.bas.point(x + 50, y + 50),
            self.bas.point(x, y + 50),
            self.bas.point(x, y)
        ])
        self.bas.hplot([
            self.bas.point(x + 10, y + 10),
            self.bas.point(x + 10, y + 40),
            self.bas.point(x + 40, y + 40),
            self.bas.point(x + 40, y + 10),
            self.bas.point(x + 10, y + 10),
            self.bas.point(x + 40, y + 40)
        ])
        self.bas.hplot([
            self.bas.point(x + 10, y + 40),
            self.bas.point(x + 40, y + 10)
        ])

