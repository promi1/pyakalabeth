import math
from viewport import Viewport


class Perspective:
    def __init__(self, viewport, distance):
        assert viewport
        assert distance >= 0
        center_x = viewport.width // 2 - 1
        center_y = viewport.height // 2 - 1
        if distance == 0:
            half_viewport = Viewport(center_x, center_y)
            self.distance = distance
            self.left = 0
            self.right = viewport.width - 1
            self.top = 0
            self.bottom = viewport.height - 1
            self.half_viewport = half_viewport
        else:
            half_viewport = Viewport(viewport.width // 2, viewport.height // 2)
            scale_ratio = self._get_scale_ratio(distance * 2)
            half_viewport = self._scale_viewport(half_viewport, scale_ratio)
            self.distance = distance
            self.left = center_x - half_viewport.width
            self.right = center_x + half_viewport.width
            self.top = center_y - half_viewport.height
            self.bottom = center_y + half_viewport.height
            self.half_viewport = half_viewport

    @staticmethod
    def _get_scale_ratio(distance):
        assert distance > 0
        angle = math.atan(1 / distance)
        angle_45_degrees = math.atan(1)
        return angle / angle_45_degrees

    @staticmethod
    def _scale_viewport(viewport, width_scale_ratio):
        assert viewport.width > 0
        assert viewport.height > 0
        aspect_ratio = viewport.height / viewport.width
        scaled_width = math.ceil(viewport.width * width_scale_ratio)
        scaled_height = math.floor(scaled_width * aspect_ratio)
        return Viewport(scaled_width, scaled_height)
