import time

from init import Init
from world_generator import WorldGenerator
from turn import Turn


# noinspection PyMethodMayBeStatic
class Akalabeth:
    player_name = ''
    difficulty = 0
    # F = Fighter or M = Mage
    player_class = ''
    character_attribute_names = []
    character_attribute_values = [0] * 6
    item_names = [''] * 6
    player_items = [0] * 6
    world_player_pos_x = 0
    world_player_pos_y = 0
    quest_monster = 0
    dungeon_player_pos_x = 0
    dungeon_player_pos_y = 0
    dungeon_player_dir_x = 0
    dungeon_player_dir_y = 0
    dungeon_bonus_hit_points = 0
    pause_after_action = False

    def __init__(self, bas, world, dungeon, monster_names, dungeon_renderer, skip_char_generator):
        self.bas = bas
        self.world = world
        self.dungeon = dungeon
        self.monster_names = monster_names
        self.dungeon_renderer = dungeon_renderer
        self.skip_char_generator = skip_char_generator

    def run(self):
        # Rerun after game over
        while True:
            self.bas.text()
            self.dungeon.level = 0
            # 7
            self.bas.clear()
            self.init_game()
            self.show_greeting()
            world_generator = WorldGenerator(self.bas, self.world)
            self.world_player_pos_x, self.world_player_pos_y = world_generator.run()
            self._show_loading_message(not self.skip_char_generator)
            self.setup_main_prompt()
            self.run_turn()
            self.game_over()

    def _show_loading_message(self, enable_sleep):
        self.bas.vtab(23)
        self.bas.print_no_newline('  (PLEASE WAIT)')
        for x in range(1, 20):
            self.bas.print_no_newline('.')
            if enable_sleep:
                time.sleep(0.1)

    def setup_main_prompt(self):
        self.bas.home()
        # 3 = White
        self.bas.hcolor(3)
        self.bas.poke(34, 20)
        self.bas.poke(33, 29)
        self.bas.home()

    def show_greeting(self):
        # 10
        self.bas.text()
        self.bas.home()
        self.bas.normal()
        self.bas.vtab(12)
        self.bas.print(' WELCOME TO AKALABETH, WORLD OF DOOM!')
        time.sleep(1)

    def run_turn(self):
        # 1000 - 1700
        turn = Turn(self.bas, self, self.dungeon, self.dungeon_renderer)
        turn.run()

    def game_over(self):
        # 6000 - 6060
        self.bas.poke(33, 40)
        self.bas.print()
        self.bas.print()
        self.bas.print('        WE MOURN THE PASSING OF ')
        if self.bas.len(self.player_name) > 22:
            self.player_name = ''
        if self.player_name == '':
            self.player_name = 'THE PEASANT'
        self.player_name += ' AND HIS COMPUTER'
        self.bas.htab(20 - self.bas.int(self.bas.len(self.player_name) / 2))
        self.bas.print(self.player_name)
        self.bas.print('  TO INVOKE A MIRACLE OF RESURRECTION')
        self.bas.print_no_newline('             <HIT SPACE>')
        while True:
            if self.bas.peek(-16384) == ' ':
                break

    def init_game(self):
        # 60000 - 60075
        init = Init(self.bas, self, self.skip_char_generator)
        init.run()

