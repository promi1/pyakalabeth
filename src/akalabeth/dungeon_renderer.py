from constants import *
from dungeon_object import DungeonObject
from viewport import Viewport
from perspective import Perspective

from chest import draw_chest, draw_chest_front_side
from door import CenterDoorComponents, SideDoorComponents, draw_center_door, draw_left_door_far, draw_left_door_near, \
    draw_right_door_far, draw_right_door_near
from ladder import HoleComponents, LadderComponents, draw_up_hole, draw_down_hole, draw_ladder
from monster import draw_monster
from wall import draw_center_wall, draw_left_corridor, draw_left_wall_segment, draw_right_corridor, \
    draw_right_wall_segment


class DungeonRenderer:
    def __init__(self, bas, dungeon, monster_names):
        self.viewport = Viewport(VIEWPORT_WIDTH, VIEWPORT_HEIGHT)
        self.max_distance = MAX_DISTANCE
        self.perspectives = [
            Perspective(self.viewport, dis) for dis in range(0, self.max_distance + 1)
        ]
        self.center_door_components = [
            CenterDoorComponents(self.perspectives[dis]) for dis in range(0, self.max_distance)
        ]
        self.side_door_components = [
            SideDoorComponents(self.perspectives[dis],
                               self.perspectives[dis + 1]) for dis in range(0, self.max_distance)
        ]
        self.hole_components = [
            HoleComponents(self.perspectives[dis],
                           self.perspectives[dis + 1]) for dis in range(0, self.max_distance)
        ]
        self.ladder_components = [
            LadderComponents(self.hole_components[dis]) for dis in range(0, self.max_distance)
        ]
        self.bas = bas
        self.dungeon = dungeon
        self.monster_names = monster_names

    def run(self, player_x, player_y, dir_x, dir_y):
        self.bas.hgr()
        dis = 0
        self.bas.hcolor(3)
        while not self._sub202(player_x, player_y, dir_x, dir_y, dis):
            dis += 1
            if dis == 10:
                print("Error: distance is above 9")
                break

    def _sub202(self, player_x, player_y, dir_x, dir_y, dis):
        plot = self.bas.hplot
        point = self.bas.point
        center_door_components = self.center_door_components[dis]
        side_door_components = self.side_door_components[dis]
        ladder_components = self.ladder_components[dis]
        hole_components = self.hole_components[dis]
        center_y_offset = self.perspectives[dis].half_viewport.height
        end = False
        center, left, right, monster = self._get_center_left_right_monster(player_x, player_y, dir_x, dir_y, dis)
        per_1 = self.perspectives[dis]
        per_2 = self.perspectives[dis + 1]
        if dis != 0 and center in [DungeonObject.WALL, DungeonObject.HIDDEN_DOOR, DungeonObject.NORMAL_DOOR]:
            end = True
            draw_center_wall(plot, point, per_1)
            if center == DungeonObject.NORMAL_DOOR:
                draw_center_door(plot, point, center_door_components)
        if not end:
            if left in [DungeonObject.WALL, DungeonObject.HIDDEN_DOOR, DungeonObject.NORMAL_DOOR]:
                draw_left_wall_segment(plot, point, per_1, per_2)
            if right in [DungeonObject.WALL, DungeonObject.HIDDEN_DOOR, DungeonObject.NORMAL_DOOR]:
                draw_right_wall_segment(plot, point, per_1, per_2)
            if left == DungeonObject.NORMAL_DOOR and dis > 0:
                draw_left_door_far(plot, point, side_door_components)
            if left == DungeonObject.NORMAL_DOOR and dis == 0:
                draw_left_door_near(plot, point, side_door_components)
            if right == DungeonObject.NORMAL_DOOR and dis > 0:
                draw_right_door_far(plot, point, side_door_components)
            if right == DungeonObject.NORMAL_DOOR and dis == 0:
                draw_right_door_near(plot, point, side_door_components)
            if not (left in [DungeonObject.WALL, DungeonObject.HIDDEN_DOOR, DungeonObject.NORMAL_DOOR]) and dis != 0:
                draw_left_corridor(plot, point, per_1, per_2)
            if not (right in [DungeonObject.WALL, DungeonObject.HIDDEN_DOOR, DungeonObject.NORMAL_DOOR]) and dis != 0:
                draw_right_corridor(plot, point, per_1, per_2)
            if center == DungeonObject.DOWN_LADDER or center == DungeonObject.DOWN_HOLE:
                draw_down_hole(plot, point, hole_components)
            if center == DungeonObject.UP_LADDER:
                draw_up_hole(plot, point, hole_components)
            if center == DungeonObject.DOWN_LADDER or center == DungeonObject.UP_LADDER:
                draw_ladder(plot, point, ladder_components)
            if center == DungeonObject.CHEST and dis > 0:
                draw_chest_front_side(plot, point, per_1)
                self.bas.inverse()
                self.bas.print('CHEST!')
                self.bas.normal()
                draw_chest(plot, point, per_1)
        if monster > 0:
            monster_pos_y = VIEWPORT_CENTER_Y + center_y_offset
            monster_pos_x = VIEWPORT_CENTER_X
            self.bas.inverse()
            if monster == 8:
                self.bas.print_no_newline('CHEST!')
            else:
                self.bas.print_no_newline(self.monster_names[monster - 1])
            self.bas.call(-868)
            self.bas.print()
            self.bas.normal()
            if dis != 0:
                draw_monster(plot, point, monster, monster_pos_x, monster_pos_y, per_1)
        return end

    def _get_center_left_right_monster(self, player_x, player_y, dir_x, dir_y, dis):
        center, monster = self._get_center_monster(player_x, player_y, dir_x, dir_y, dis)
        left = self._get_left(player_x, player_y, dir_x, dir_y, dis)
        right = self._get_right(player_x, player_y, dir_x, dir_y, dis)
        return center, left, right, monster

    def _get_right(self, player_x, player_y, dir_x, dir_y, dis):
        right_x = player_x + dir_x * dis - dir_y
        right_y = player_y + dir_y * dis + dir_x
        right = self.dungeon.map[right_x][right_y].obj
        return right

    def _get_left(self, player_x, player_y, dir_x, dir_y, dis):
        left_x = player_x + dir_x * dis + dir_y
        left_y = player_y + dir_y * dis - dir_x
        left = self.dungeon.map[left_x][left_y].obj
        return left

    def _get_center_monster(self, player_x, player_y, dir_x, dir_y, dis):
        center_x = player_x + dir_x * dis
        center_y = player_y + dir_y * dis
        map_pair = self.dungeon.map[center_x][center_y]
        monster = map_pair.monster
        center = map_pair.obj
        return center, monster
