from dungeon_object import DungeonObject


class DungeonGenerator:
    def __init__(self, bas, dungeon, difficulty):
        self.bas = bas
        self.dungeon = dungeon
        self.difficulty = difficulty
        self.player_pos_x = 1
        self.player_pos_y = 1

    def run(self, player_pos_x, player_pos_y):
        self.player_pos_x = player_pos_x
        self.player_pos_y = player_pos_y
        self._clear_dungeon_inside()
        self._add_wall_around_dungeon()
        self._add_room_walls()
        self._add_random_dungeon_items()
        self._add_dungeon_ladders()
        self._add_dungeon_monsters()

    def _clear_dungeon_inside(self):
        dungeon_map = self.dungeon.map
        for x in range(1, 10):
            for y in range(1, 10):
                dungeon_map[x][y].obj = DungeonObject.EMPTY0
                dungeon_map[x][y].monster = 0

    def _add_wall_around_dungeon(self):
        dungeon_map = self.dungeon.map
        for x in range(0, 11):
            dungeon_map[x][0].obj = DungeonObject.WALL
            dungeon_map[x][0].monster = 0
            dungeon_map[x][10].obj = DungeonObject.WALL
            dungeon_map[x][10].monster = 0
            dungeon_map[0][x].obj = DungeonObject.WALL
            dungeon_map[0][x].monster = 0
            dungeon_map[10][x].obj = DungeonObject.WALL
            dungeon_map[10][x].monster = 0

    def _add_room_walls(self):
        dungeon_map = self.dungeon.map
        for x in range(2, 9, 2):
            for y in range(1, 10):
                dungeon_map[x][y].obj = DungeonObject.WALL
                dungeon_map[y][x].obj = DungeonObject.WALL

    def _add_random_dungeon_items(self):
        rnd = self.bas.rnd
        dungeon_map = self.dungeon.map
        for x in range(2, 9, 2):
            for y in range(1, 10, 2):
                if rnd(1) > .95:
                    dungeon_map[x][y].obj = DungeonObject.TRAP
                if rnd(1) > .95:
                    dungeon_map[y][x].obj = DungeonObject.TRAP
                if rnd(1) > .6:
                    dungeon_map[y][x].obj = DungeonObject.HIDDEN_DOOR
                if rnd(1) > .6:
                    dungeon_map[x][y].obj = DungeonObject.HIDDEN_DOOR
                if rnd(1) > .6:
                    dungeon_map[x][y].obj = DungeonObject.NORMAL_DOOR
                if rnd(1) > .6:
                    dungeon_map[y][x].obj = DungeonObject.NORMAL_DOOR
                if rnd(1) > .97:
                    dungeon_map[y][x].obj = DungeonObject.DOWN_HOLE
                if rnd(1) > .97:
                    dungeon_map[x][y].obj = DungeonObject.DOWN_HOLE
                if rnd(1) > .94:
                    dungeon_map[x][y].obj = DungeonObject.CHEST
                if rnd(1) > .94:
                    dungeon_map[y][x].obj = DungeonObject.CHEST

    def _add_dungeon_ladders(self):
        dungeon_map = self.dungeon.map
        level = self.dungeon.level
        dungeon_map[2][1].obj = DungeonObject.EMPTY0
        even = level / 2 == self.bas.int(level / 2)
        if even:
            dungeon_map[7][3].obj = DungeonObject.DOWN_LADDER
            dungeon_map[3][7].obj = DungeonObject.UP_LADDER
        else:
            dungeon_map[7][3].obj = DungeonObject.UP_LADDER
            dungeon_map[3][7].obj = DungeonObject.DOWN_LADDER
        if level == 1:
            dungeon_map[1][1].obj = DungeonObject.UP_LADDER
            dungeon_map[7][3].obj = DungeonObject.EMPTY0

    def _add_dungeon_monsters(self):
        dungeon = self.dungeon
        level = dungeon.level
        for kind in range(1, 11):
            monster = dungeon.monsters[kind]
            location = dungeon.monster_locations[kind]
            # Lower dungeon floors get more monsters, but also randomize the amount
            if kind - 2 > level or self.bas.rnd(1) > .4:
                monster[0] = 0
                monster[1] = kind + 3 + level
            else:
                monster[0] = 1
                monster[1] = kind * 2 + level * 2 * self.difficulty
                self._set_monster_location(location)
                dungeon.map[location[0]][location[1]].monster = kind

    def _set_monster_location(self, location):
        while True:
            if self._try_set_monster_location(location):
                break

    def _try_set_monster_location(self, location):
        pos_x = self.bas.int(self.bas.rnd(1) * 9 + 1)
        pos_y = self.bas.int(self.bas.rnd(1) * 9 + 1)
        if self.dungeon.map[pos_x][pos_y].obj != DungeonObject.EMPTY0:
            return False
        if pos_x == self.player_pos_x and pos_y == self.player_pos_y:
            return False
        location[0] = pos_x
        location[1] = pos_y
        return True
