class Dungeon:
    def __init__(self, map):
        # inside or outside the dungeon, also how deep into the dungeon
        self.level = 0
        # monster alive/present and kind in the dungeon
        self.monsters = [[0] * 2 for _ in range(0, 11)]
        self.monster_locations = [[0] * 2 for _ in range(0, 11)]
        self.map = map
