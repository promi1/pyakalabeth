from dungeon_object import DungeonObject


class Monsters:
    def __init__(self, bas, main, dungeon):
        self.bas = bas
        self.main = main
        self.dungeon = dungeon

    def run(self):
        for kind in range(1, 11):
            self.run_monster(kind)

    def run_monster(self, kind):
        monster = self.dungeon.monsters[kind]
        location = self.dungeon.monster_locations[kind]
        # Present?
        if monster[0] == 0:
            return
        # Distance to player
        ra = self.monster_distance_to_player(location)
        close_by = ra < 1.3
        in_mimic_stealth_range = ra < 3
        is_weak = self.monster_weak(monster[1])
        # Monster is strong enough to attack
        if not is_weak:
            if close_by:
                self.encounter_monster(kind)
                return
            # Mimic and in stealth range
            if kind == 8 and in_mimic_stealth_range:
                return
        # Since it did not attack, move more closely towards the player
        if self.move_monster(kind, location, is_weak):
            return
        # Is monster too weak (HP) to attack
        if is_weak:
            #  Still near the player? Attack anyway!
            if close_by:
                self.encounter_monster(kind)
            # far away, heal monster
            else:
                monster[1] += kind + self.dungeon.level

    def monster_distance_to_player(self, location):
        delta_x = self.main.dungeon_player_pos_x - location[0]
        delta_y = self.main.dungeon_player_pos_y - location[1]
        return self.pythagoras(delta_x, delta_y)

    def pythagoras(self, a, b):
        return self.bas.sqr(a ** 2 + b ** 2)

    def move_monster(self, kind, location, is_weak):
        x = self.bas.sgn(self.main.dungeon_player_pos_x - location[0])
        y = self.bas.sgn(self.main.dungeon_player_pos_y - location[1])
        # Monster is too weak to attack, move away instead of towards
        if is_weak:
            x = -x
            y = -y
        # What is next to the new position when moving in Y direction?
        dy = self.dungeon.map[location[0]][int(location[1] + y + .5)]
        # Is it a wall, a trap or above 9?
        y_axis_move_forbidden = dy.obj == DungeonObject.WALL or dy.monster > 0 or dy.obj == DungeonObject.TRAP
        # What is next to the new position when moving in X direction?
        dx = self.dungeon.map[int(location[0] + x + .5)][location[1]]
        # Is it a wall, a trap or above 9?
        x_axis_move_forbidden = dx.obj == DungeonObject.WALL or dx.monster > 0 or dx.obj == DungeonObject.TRAP

        # Move in x direction, y direction or not at all?
        if y != 0 and not y_axis_move_forbidden:
            x = 0
        else:
            y = 0
            if x != 0 and x_axis_move_forbidden:
                x = 0

        move = x != 0 or y != 0
        if move:
            # Remove monster from its current position
            self.dungeon.map[location[0]][location[1]].monster = 0
            # Is the target actually the player position?
            # -> Skip this monster
            # TODO: Monster is already removed from dungeon. Bug?!
            if location[0] + x == self.main.dungeon_player_pos_x and location[1] + y == self.main.dungeon_player_pos_y:
                return False
            # Move monster
            location[0] += x
            location[1] += y
            # Place monster in new location
            self.dungeon.map[location[0]][location[1]].monster = kind
            return True
        else:
            return False

    def monster_weak(self, hp):
        return hp < self.dungeon.level * self.main.difficulty

    def encounter_monster(self, monster):
        # 4500
        if (monster == 2 or monster == 7) and (self.bas.rnd(1) < .5):
            if monster == 7:
                self.steel_food()
            else:
                self.steel_item()
        else:
            self.monster_attack(monster)
        if self.main.pause_after_action:
            self.bas.print_no_newline('-CR- TO CONT. ')
            self.bas.input()

    def monster_attack(self, monster):
        self.bas.print('YOU ARE BEING ATTACKED')
        self.bas.print(f'BY A {self.main.monster_names[monster - 1]}')
        roll = self.bas.rnd(1) * 20
        shield = self.bas.sgn(self.main.player_items[3])
        stamina = self.main.character_attribute_values[3]
        attack_strength = monster + self.dungeon.level
        defense_strength = shield + stamina
        if roll - defense_strength + attack_strength < 0:
            self.bas.print('MISSED')
        else:
            self.bas.print('HIT')
            self.main.character_attribute_values[0] = self.main.character_attribute_values[0] - self.bas.int(
                self.bas.rnd(1) * monster + self.dungeon.level)

    def steel_food(self):
        self.main.player_items[0] = self.bas.int(self.main.player_items[0] / 2)
        self.bas.print('A GREMLIN STOLE SOME FOOD')

    def steel_item(self):
        while True:
            zz = self.bas.int(self.bas.rnd(1) * 6)
            if self.main.player_items[zz] >= 1:
                break
        self.main.player_items[zz] -= 1
        self.bas.print(f'A THIEF STOLE A {self.main.item_names[zz]}')
