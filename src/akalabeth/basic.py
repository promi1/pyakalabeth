import math
import random
import time
from collections import namedtuple
from enum import Enum
from skimage.draw import line


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class VideoMode(Enum):
    TEXT = 0
    HGR = 1


# noinspection PyMethodMayBeStatic
class Basic:
    def __init__(self):
        self.text_buffer_columns = 40
        self.text_buffer_lines = 24
        self.hgr_buffer_width = 280
        self.hgr_buffer_height = 192
        self.video_mode = VideoMode.TEXT
        self.cursor = self.point(0, 0)
        self.text_buffer = self._get_empty_text_buffer()
        self.hgr_buffer = self._get_empty_hgr_buffer()
        self.hgr_color = 0
        self.key = None
        self.text_window_top = 0
        self.text_window_bottom = self.text_buffer_lines
        self.text_window_left = 0
        self.text_window_right = self.text_buffer_columns

    def abs(self, val):
        if val < 0:
            return val * -1
        else:
            return val

    def call(self, addr):
        if addr == -868:
            self._clear_text_line_from_cursor_to_right()
        elif addr == 62450:
            self._clear_hgr_buffer()
        else:
            raise ValueError('Unsupported CALL')

    def _get_empty_hgr_buffer(self):
        return [[0] * self.hgr_buffer_width for i in range(self.hgr_buffer_height)]

    def _clear_hgr_buffer(self):
        self.hgr_buffer = self._get_empty_hgr_buffer()

    def clear(self):
        assert self.video_mode == VideoMode.TEXT
        self._clear_text_buffer()

    def _get_empty_text_buffer(self):
        return [[' '] * self.text_buffer_columns for i in range(self.text_buffer_lines)]

    def _clear_text_buffer(self):
        self.text_buffer = self._get_empty_text_buffer()

    def _clear_text_line_from_cursor_to_right(self):
        for x in range(self.cursor.x, self.text_window_right):
            self.text_buffer[self.cursor.y][x] = ' '

    def _clear_text_window(self):
        for x in range(self.text_window_left, self.text_window_right):
            for y in range(self.text_window_top, self.text_window_bottom):
                self.text_buffer[y][x] = ' '

    def rnd(self, rng):
        if rng == 1:
            return random.random()
        else:
            raise ValueError('rnd only supports the 0..1 range for now')

    def sgn(self, num):
        if num < 0:
            return -1
        elif num == 0:
            return 0
        elif num > 0:
            return 1

    def sqr(self, num):
        return math.sqrt(num)

    def atn(self, num):
        return math.atan(num)

    # Enter text video mode (40 lines with 40 characters per line)
    def text(self):
        self.video_mode = VideoMode.TEXT
        self.text_window_left = 0
        self.text_window_right = self.text_buffer_columns
        self.text_window_top = 0
        self.text_window_bottom = self.text_buffer_lines

    # Enter high resolution graphics video mode (280x160 pixels + 40x4 text area at the bottom)
    def hgr(self):
        self.video_mode = VideoMode.HGR
        self._clear_hgr_buffer()

    def hcolor(self, color):
        self.hgr_color = color

    def point(self, x, y):
        return Point(x, y)

    # Plot a line in high resolution graphics mode
    def hplot(self, points):
        for i in range(0, len(points) - 1):
            pt1 = points[i]
            pt2 = points[i + 1]
            rr, cc = line(pt1.x, pt1.y, pt2.x, pt2.y)
            line_pts = list(zip(rr, cc))
            for line_pt in line_pts:
                self.hgr_buffer[line_pt[1]][line_pt[0]] = 1

    def home(self):
        self._clear_text_window()
        self.cursor.x = self.text_window_left
        self.cursor.y = self.text_window_top
        pass

    def normal(self):
        # TODO: Implement normal + inverse
        pass

    def inverse(self):
        # TODO: Implement normal + inverse
        pass

    def vtab(self, val):
        self.cursor.y = val - 1

    def htab(self, val):
        self.cursor.x = val - 1

    def tab(self, val):
        self.htab(val)

    def print(self, val=''):
        self.print_no_newline(val)
        self._new_line()

    def print_no_newline(self, val):
        for c in str(val):
            self.text_buffer[self.cursor.y][self.cursor.x] = c
            self.cursor.x += 1
            if self.cursor.x >= self.text_buffer_columns:
                self._new_line()

    def _new_line(self):
        self.cursor.x = self.text_window_left
        if self.cursor.y < self.text_window_bottom - 1:
            self.cursor.y += 1
        else:
            for y in range(self.text_window_top, self.text_window_bottom):
                for x in range(self.text_window_left, self.text_window_right - 1):
                    if y < self.text_window_bottom - 1:
                        self.text_buffer[y][x] = self.text_buffer[y + 1][x]
                    else:
                        self.text_buffer[y][x] = ' '

    def input(self, prompt=''):
        self.print_no_newline(prompt)
        s = ''
        while True:
            c = self.get()
            if c == '\n':
                break
            s = s + c
            self.print_no_newline(c)
        self._new_line()
        return s

    def get(self):
        while not self.key:
            time.sleep(0.1)
        key = self.key
        self.key = None
        return key

    def _read_keyboard(self):
        while not self.key:
            time.sleep(0.1)
        key = self.key
        return key

    def _clear_keyboard(self):
        self.key = None

    def int(self, val):
        return int(val)

    def val(self, num):
        try:
            return float(num)
        except ValueError:
            return .0
        except OverflowError:
            return .0

    def peek(self, adr):
        if adr == -16384:
            return self._read_keyboard()
        else:
            raise ValueError('Unsupported PEEK address')

    def poke(self, adr, val):
        if adr == 32:
            self.text_window_left = val
        elif adr == 33:
            self.text_window_right = val
        elif adr == 34:
            self.text_window_top = val
        elif adr == 35:
            self.text_window_bottom = val
        elif adr == -16368:
            self._clear_keyboard()
        else:
            raise ValueError('Unsupported POKE address')

    def len(self, val):
        return len(val)

    def str(self, val):
        return str(val)
