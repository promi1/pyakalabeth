from enum import Enum


class WorldObject(Enum):
    EMPTY = 0
    MOUNTAIN = 1
    # ??? (A square, could be a field or a house, etc.)
    FIELD = 2
    # A town only has a single shop in this game
    TOWN = 3
    DUNGEON = 4
    # Lord British's castle
    CASTLE = 5
