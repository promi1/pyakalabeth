from constants import *


class HoleComponents:
    def __init__(self, perspective_1, perspective_2):
        assert perspective_1
        assert perspective_2
        center_x_offset_1 = perspective_1.half_viewport.width
        center_y_offset_1 = perspective_1.half_viewport.height
        center_x_offset_2 = perspective_2.half_viewport.width
        center_y_offset_2 = perspective_2.half_viewport.height
        self.near_left = VIEWPORT_CENTER_X - center_x_offset_1 // 3
        self.near_right = VIEWPORT_CENTER_X + center_x_offset_1 // 3
        self.far_left = VIEWPORT_CENTER_X - center_x_offset_2 // 3
        self.far_right = VIEWPORT_CENTER_X + center_x_offset_2 // 3
        self.top = VIEWPORT_CENTER_Y + (center_y_offset_1 * 2 + center_y_offset_2) // 3
        self.bottom = VIEWPORT_CENTER_Y + (center_y_offset_1 + 2 * center_y_offset_2) // 3


class LadderComponents:
    def __init__(self, hole_components):
        assert hole_components
        self.left = (hole_components.near_left * 2 + hole_components.near_right) // 3
        self.right = (hole_components.near_left + 2 * hole_components.near_right) // 3
        self.bottom = hole_components.top
        self.top = VIEWPORT_MAX_Y - self.bottom


def draw_down_hole(plot, point, components):
    plot([
        point(components.near_left, components.top),
        point(components.far_left, components.bottom),
        point(components.far_right, components.bottom),
        point(components.near_right, components.top),
        point(components.near_left, components.top)
    ])


def draw_up_hole(plot, point, components):
    plot([
        point(components.near_left, (VIEWPORT_MAX_Y - 1) - components.top),
        point(components.far_left, (VIEWPORT_MAX_Y - 1) - components.bottom),
        point(components.far_right, (VIEWPORT_MAX_Y - 1) - components.bottom),
        point(components.near_right, (VIEWPORT_MAX_Y - 1) - components.top),
        point(components.near_left, (VIEWPORT_MAX_Y - 1) - components.top)
    ])


def draw_ladder(plot, point, components):
    bottom = components.bottom
    top = components.top
    left = components.left
    right = components.right
    plot([
        point(left, bottom),
        point(left, top)
    ])
    plot([
        point(right, top),
        point(right, bottom)
    ])
    spoke_1 = (bottom * 4 + top * 1) // 5
    spoke_2 = (bottom * 3 + top * 2) // 5
    spoke_3 = (bottom * 2 + top * 3) // 5
    spoke_4 = (bottom * 1 + top * 4) // 5
    plot([
        point(left, spoke_1),
        point(right, spoke_1)
    ])
    plot([
        point(left, spoke_2),
        point(right, spoke_2)
    ])
    plot([
        point(left, spoke_3),
        point(right, spoke_3)
    ])
    plot([
        point(left, spoke_4),
        point(right, spoke_4)
    ])

