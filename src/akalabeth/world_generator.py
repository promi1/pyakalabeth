from world_object import WorldObject


class WorldGenerator:

    def __init__(self, bas, world):
        self.bas = bas
        self.world = world

    def run(self):
        self._surround_with_mountains()
        self._place_random_objects_except_castles()
        self._place_castle()
        return self._place_starting_town()

    def _surround_with_mountains(self):
        world = self.world
        for x in range(0, 21):
            world[x][0] = WorldObject.MOUNTAIN
            world[0][x] = WorldObject.MOUNTAIN
            world[x][20] = WorldObject.MOUNTAIN
            world[20][x] = WorldObject.MOUNTAIN

    def _place_random_objects_except_castles(self):
        for x in range(1, 20):
            for y in range(1, 20):
                world_object = self._get_random_world_object_except_castle()
                self.world[x][y] = world_object

    def _place_castle(self):
        x = self._get_random_inside_pos()
        y = self._get_random_inside_pos()
        self.world[x][y] = WorldObject.CASTLE

    def _place_starting_town(self):
        x = self._get_random_inside_pos()
        y = self._get_random_inside_pos()
        self.world[x][y] = WorldObject.TOWN
        return x, y

    def _get_random_world_object_except_castle(self):
        # Random number in range [0..4] -> Any world objects except WorldObject.CASTLE
        world_object = WorldObject(self.bas.int(self.bas.rnd(1) ** 5 * 4.5))
        if world_object == WorldObject.TOWN and self.bas.rnd(1) > .5:
            world_object = WorldObject.EMPTY
        return world_object

    def _get_random_inside_pos(self):
        return self.bas.int(self.bas.rnd(1) * 19 + 1)
