import time


# noinspection PyMethodMayBeStatic
class Shop:
    def __init__(self, bas, main):
        self.bas = bas
        self.main = main
    
    def run(self):
        # 60200 - 60250
        self.bas.home()
        self.bas.print('WELCOME TO THE ADVENTURE SHOP')
        while True:
            self.bas.print_no_newline('WHICH ITEM SHALT THOU BUY ')
            player_input = self.bas.get()
            if player_input == 'Q':
                self.bas.print()
                self.bas.print('BYE')
                time.sleep(2)
                self.bas.text()
                self.bas.home()
                break
            item, price = self.lookup_item(player_input)
            if not self.can_buy(player_input, item, price):
                continue
            self.buy(item, price)

    def can_buy(self, player_input, item, price):
        # error checking
        if item == -1:
            self.bas.print(player_input)
            self.bas.print("I'M SORRY WE DON'T HAVE THAT.")
            return False
        if player_input == 'R' and self.main.player_class == 'M':
            self.bas.print("I'M SORRY MAGES")
            self.bas.print("CAN'T USE THAT!")
            return False
        if player_input == 'B' and self.main.player_class == 'M':
            self.bas.print("I'M SORRY MAGES")
            self.bas.print("CAN'T USE THAT!")
            return False
        if self.main.character_attribute_values[5] - price < 0:
            self.bas.print("M'LORD THOU CAN NOT AFFORD THAT ITEM.")
            return False
        return True

    def buy(self, item, price):
        # Quantity of food per price is 10, for other items it is 1
        if item == 0:
            self.main.player_items[item] += 10
        else:
            self.main.player_items[item] += 1
        self.main.character_attribute_values[5] = self.main.character_attribute_values[5] - price
        self.bas.vtab(10)
        self.bas.htab(16)
        self.bas.print_no_newline(f'{self.main.character_attribute_values[5]}  ')
        # Update number of owned item
        self.bas.vtab(5 + item)
        self.bas.htab(25 - self.bas.len(self.bas.str(self.main.player_items[item])))
        self.bas.print_no_newline(self.main.player_items[item])
        self.bas.htab(1)
        self.bas.vtab(14)
        self.bas.print()

    def lookup_item(self, player_input):
        if player_input == 'F':
            self.bas.print('FOOD')
            item = 0
            price = 1
        elif player_input == 'R':
            self.bas.print('RAPIER')
            item = 1
            price = 8
        elif player_input == 'A':
            self.bas.print('AXE')
            item = 2
            price = 5
        elif player_input == 'S':
            self.bas.print('SHIELD')
            item = 3
            price = 6
        elif player_input == 'B':
            self.bas.print('BOW')
            item = 4
            price = 3
        elif player_input == 'M':
            self.bas.print('AMULET')
            item = 5
            price = 15
        else:
            item = -1
            price = 0
        return item, price
